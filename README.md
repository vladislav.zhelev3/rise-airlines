## Airlines project

This project is console application on .Net 8.0 with C#.
This aplication use 3 business classes and user commands to process the data.
There are Unit test and pipelines for them.

## Nemetschek bootcamp 2024

This project is for Nemetschek bootcamp

## Terminal commands

dotnet run - run console application

dotnet test - run tests

dotnet test --logger "console;verbosity=detailed" - run test and write console output - with this terminal command, you can run tests and see the output 
