let dataList = [];
const flightProperties = [
  "flight_number",
  "from",
  "to",
  "aircraft_model_id",
  "price",
  "duration",
  "departure_datetime",
  "arrival_datetime",
  "airline_id"
];

// http requests
export async function getFromAPI(link, render) {
  fetch(link, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .then((data) => {
      dataList = data;
      render();
    })
    .catch((error) => console.error("Error:", error));
}

export function postToAPI(link, data) {
  fetch(link, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      console.log("Success!");
    })
    .catch((error) => console.error("Error:", error));
}

export function deleteFromAPI(link) {
  fetch(link, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      } else {
        console.log("Success!");
      }
    })
    .catch((error) => console.error("Error:", error));
}

export function UpdateAPI(link, data) {
  fetch(link, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      } else {
        console.log("Success!");
      }
    })
    .catch((error) => console.error("Error:", error));
}

export async function getOnlyAPI(link) {
  try {
    const response = await fetch(link, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    const data = await response.json();
    console.log("Data fetched from API:", data);
    return data;
  } catch (error) {
    console.error("Error:", error);
    throw error;
  }
}

// buttons for edit & delete
function createDeleteButton(link, obj) {
  const deleteButton = document.createElement("button");
  deleteButton.innerHTML = "Delete";
  deleteButton.id = obj.id;
  deleteButton.addEventListener("click", function (event) {
    deleteFromAPI(link);
    location.reload();
  });
  deleteButton.classList.add("button");
  return deleteButton;
}

function createEditButton(obj, props) {
  const editButton = document.createElement("button");
  editButton.innerHTML = "Edit";
  editButton.id = obj.id;
  editButton.addEventListener("click", function (event) {
    event.preventDefault();
    editData(obj, props);
  });
  editButton.classList.add("button");
  return editButton;
}

// Edit func
function editData(data, props) {
  const form = document.getElementById("flightForm");
  const table = document.querySelector("table");
  const searchBar = document.getElementById("searchForm");
  const buttonShowMore = document.getElementById("showMoreButton");
  const legend = form.querySelector("legend");
  legend.innerText = "Edit Row";
  table.classList.add("hidden");
  searchBar.classList.add("hidden");
  buttonShowMore.classList.add("hidden");
  form.classList.remove("hidden");

  let hiddenInput = form.querySelector("input[name='id']");
  if (!hiddenInput) {
    hiddenInput = document.createElement("input");
    hiddenInput.type = "hidden";
    hiddenInput.name = "id";
    form.appendChild(hiddenInput);
  }
  hiddenInput.value = data.id;

  for (const prop of props) {
    const input = form.querySelector(`[name=${prop}]`);
    if (input) {
      input.value = data[prop];
    }
  }
}

// create rows for tables
export function createFlightRow(obj) {
  const tableRow = document.createElement("tr");
  const deleteButton = createDeleteButton(
    `https://localhost:7153/Flight?id=${obj.id}`,
    obj
  );
  const editButton = createEditButton(obj, flightProperties);

  for (const prop of flightProperties) {
    const tableData = document.createElement("td");
    tableData.classList.add("table-row-tag");
    tableData.textContent = obj[prop] ?? "N/A";
    tableRow.appendChild(tableData);
  }

  const buttonTableData = document.createElement("td");
  buttonTableData.classList.add("table-row-tag");
  buttonTableData.appendChild(deleteButton);
  buttonTableData.appendChild(editButton);
  tableRow.appendChild(buttonTableData);

  return tableRow;
}

// render the objects
export function renderFlights() {
  const objTableBody = document.getElementById("airlinesTableBody");

  objTableBody.innerHTML = "";

  dataList.forEach((obj) => {
    const row = createFlightRow(obj);
    objTableBody.appendChild(row);
  });
}

// Event listeners for form buttons
document.addEventListener("DOMContentLoaded", () => {
  const showFormButton = document.getElementById("showFormButton");
  const formSection = document.getElementById("formSection");
  const flightForm = document.getElementById("flightForm");
  const submitButton = document.getElementById("submitButton");

  showFormButton.addEventListener("click", () => {
    formSection.classList.toggle("hidden");
  });

  flightForm.addEventListener("submit", (event) => {
    event.preventDefault();
    const formData = new FormData(flightForm);
    const data = Object.fromEntries(formData.entries());

    if (data.id) {
      UpdateAPI(`https://localhost:7153/Flight?id=${data.id}`, data);
    } else {
      postToAPI("https://localhost:7153/Flight", data);
    }

    location.reload();
  });

  const searchButton = document.getElementById("searchButton");
  searchButton.addEventListener("click", (event) => {
    event.preventDefault();
    const searchInput = document.getElementById("searchInput").value;
    const filterDropdown = document.getElementById("filterDropdown").value;
    getFromAPI(`https://localhost:7153/Flight?${filterDropdown}=${searchInput}`, renderFlights);
  });

  getFromAPI("https://localhost:7153/Flight", renderFlights);
});
