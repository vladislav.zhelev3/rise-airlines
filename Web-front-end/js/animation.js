                        // toggle button

document.addEventListener('DOMContentLoaded', function() {
    var showFormButton = document.getElementById('showFormButton');
    var formSection = document.getElementById('formSection');
    var tableSection = document.querySelector('.table');
    var tableRows = document.querySelectorAll('.table tbody tr');
    var showMoreButton = document.getElementById('showMoreButton');
    var containerSection = document.querySelector('.container');
    tableSection.style.display = 'block';
    formSection.style.display = 'none';
    var hideRows = true;

    if (tableRows.length > 3) {
        tableRows.forEach(function(row, index) {
            if (index >= 3) {
                row.style.display = 'none';
            }
        });
        // tableSection.parentNode.appendChild(showMoreButton);
    }

    showFormButton.addEventListener('click', function() {
        if (formSection.style.display === 'none') {
            formSection.style.display = 'block';
            tableSection.style.display = 'none';
            containerSection.style.display = 'none';
            showMoreButton.style.display = 'none';
            showFormButton.textContent = 'Show Table';
        } else {
            formSection.style.display = 'none';
            tableSection.style.display = 'block';
            containerSection.style.display = '';
            showMoreButton.style.display = '';
            showFormButton.textContent = 'Show Form';
        }
    });

    showMoreButton.addEventListener('click', function() {
        tableRows.forEach(function(row, index) {
            if (index >= 3) {
                row.style.display = hideRows ? 'table-row' : 'none';
            }
        });
        showMoreButton.textContent = hideRows ? 'Show Less' : 'Show More';
        hideRows = !hideRows;
    });
});

                            // Button animation

var scrollToTopBtn = document.getElementById("scrollToTopBtn");

window.onscroll = function () {
    scrollFunction();
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        scrollToTopBtn.style.display = "block";
    } else {
        scrollToTopBtn.style.display = "none";
    }
}

scrollToTopBtn.addEventListener("click", function () {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
});