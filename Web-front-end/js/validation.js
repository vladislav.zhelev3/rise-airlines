// /*flight validation*/

// document.addEventListener('DOMContentLoaded', function () {
//     const flightForm = document.getElementById('flightForm');
//     const submitButton = document.getElementById('submitButton');
//     document.getElementById("submitButton").disabled = true;

//     function validateFlightNumber() {
//         const flightNumberInput = document.getElementById('flight_number');
//         const flightNumberError = document.getElementById('flightNumberError');
//         const flightNumber = flightNumberInput.value.trim();

//         if (flightNumber === '') {
//             flightNumberError.textContent = 'Flight number is required';
//             flightNumberError.style.color = 'red';

//             return false;
//         } else {
//             flightNumberError.textContent = '';
//             return true;
//         }
//     }

//     function validateFromAirportId() {
//         const fromAirportIdInput = document.getElementById('from');
//         const fromAirportIdError = document.getElementById('fromAirportIdError');
//         const fromAirportId = fromAirportIdInput.value.trim();

//         if (fromAirportId === '') {
//             fromAirportIdError.textContent = 'From airport id is required';
//             fromAirportIdError.style.color = 'red';

//             return false;
//         } else {
//             fromAirportIdError.textContent = '';
//             return true;
//         }
//     }

//     function validateToAirportId() {
//         const toAirportIdInput = document.getElementById('to');
//         const toAirportIdError = document.getElementById('toAirportIdError');
//         const toAirportId = toAirportIdInput.value.trim();

//         if (toAirportId === '') {
//             toAirportIdError.textContent = 'To airport id is required';
//             toAirportIdError.style.color = 'red';

//             return false;
//         } else {
//             toAirportIdError.textContent = '';
//             return true;
//         }
//     }

//     function validateAircraftModelId() {
//         const aircraftModelIdInput = document.getElementById('aircraft_model_id');
//         const aircraftModelIdError = document.getElementById('aircraftModelIdError');
//         const aircraftModelId = aircraftModelIdInput.value.trim();

//         if (aircraftModelId === '') {
//             aircraftModelIdError.textContent = 'Aircraft model id is required';
//             aircraftModelIdError.style.color = 'red';

//             return false;
//         } else {
//             aircraftModelIdError.textContent = '';
//             return true;
//         }
//     }

//     function validatePrice() {
//         const priceInput = document.getElementById('price');
//         const priceError = document.getElementById('priceError');
//         const price = priceInput.value.trim();

//         if (price === '') {
//             priceError.textContent = 'Price is required';
//             priceError.style.color = 'red';

//             return false;
//         } else {
//             priceError.textContent = '';
//             return true;
//         }
//     }

//     function validateDuration() {
//         const durationInput = document.getElementById('duration');
//         const durationError = document.getElementById('durationError');
//         const duration = durationInput.value.trim();

//         if (duration === '') {
//             durationError.textContent = 'Duration is required';
//             durationError.style.color = 'red';

//             return false;
//         } else {
//             durationError.textContent = '';
//             return true;
//         }
//     }

//     function validateDepartureDatetime() {
//         const departureDatetimeInput = document.getElementById('departure_datetime');
//         const departureDatetimeError = document.getElementById('departureDatetimeError');
//         const departureDatetime = departureDatetimeInput.value.trim();

//         if (departureDatetime === '') {
//             departureDatetimeError.textContent = 'Departure datetime is required';
//             departureDatetimeError.style.color = 'red';

//             return false;
//         } else {
//             departureDatetimeError.textContent = '';
//             return true;
//         }
//     }

//     function validateArrivalDatetime() {
//         const arrivalDatetimeInput = document.getElementById('arrival_datetime');
//         const arrivalDatetimeError = document.getElementById('arrivalDatetimeError');
//         const arrivalDatetime = arrivalDatetimeInput.value.trim();

//         if (arrivalDatetime === '') {
//             arrivalDatetimeError.textContent = 'Arrival datetime is required';
//             arrivalDatetimeError.style.color = 'red';

//             return false;
//         } else {
//             arrivalDatetimeError.textContent = '';
//             return true;
//         }
//     }

//     function validateAirlineId() {
//         const airlineIdInput = document.getElementById('airline_id');
//         const airlineIdError = document.getElementById('airlineIdError');
//         const airlineId = airlineIdInput.value.trim();

//         if (airlineId === '') {
//             airlineIdError.textContent = 'Airline id is required';
//             airlineIdError.style.color = 'red';

//             return false;
//         } else {
//             airlineIdError.textContent = '';
//             return true;
//         }
//     }

//     document.getElementById('flight_number').addEventListener('blur', validateFlightNumber);
//     document.getElementById('from').addEventListener('blur', validateFromAirportId);
//     document.getElementById('to').addEventListener('blur', validateToAirportId);
//     document.getElementById('aircraft_model_id').addEventListener('blur', validateAircraftModelId);
//     document.getElementById('price').addEventListener('blur', validatePrice);
//     document.getElementById('duration').addEventListener('blur', validateDuration);
//     document.getElementById('departure_datetime').addEventListener('blur', validateDepartureDatetime);
//     document.getElementById('arrival_datetime').addEventListener('blur', validateArrivalDatetime);
//     document.getElementById('airline_id').addEventListener('blur', validateAirlineId);

//     flightForm.addEventListener('submit', function (event) {
//         if (!validateFlightNumber() &&
//             !validateFromAirportId() &&
//             !validateToAirportId() &&
//             !validateAircraftModelId() &&
//             !validatePrice() &&
//             !validateDuration() &&
//             !validateDepartureDatetime() &&
//             !validateArrivalDatetime() &&
//             !validateAirlineId()) {
//             event.preventDefault();
//         }
//     });

//     function updateSubmitButton() {
//         if (flightForm.checkValidity()) {
//             submitButton.disabled = false;
//         } else {
//             submitButton.disabled = true;
//         }
//     }

//     flightForm.addEventListener('input', updateSubmitButton);
// });


/*airline validation*/

document.addEventListener('DOMContentLoaded', function () {
    const flightForm = document.getElementById('flightForm');
    const submitButton = document.getElementById('submitButton');
    document.getElementById("submitButton").disabled = true;

    function validateName() {
        const nameInput = document.getElementById('name');
        const nameError = document.getElementById('nameError');
        const name = nameInput.value.trim();

        if (name === '') {
            nameError.textContent = 'Name is required';
            nameError.style.color = 'red';

            return false;
        } else {
            nameError.textContent = '';
            return true;
        }
    }

    function validateFounded() {
        const foundedInput = document.getElementById('founded');
        const foundedError = document.getElementById('foundedError');
        const founded = foundedInput.value.trim();

        if (founded === '') {
            foundedError.textContent = 'Founded id is required';
            foundedError.style.color = 'red';

            return false;
        } else {
            foundedError.textContent = '';
            return true;
        }
    }

    function validateFleetSize() {
        const fleetSizeInput = document.getElementById('fleet_size');
        const fleetSizeError = document.getElementById('fleetSizeError');
        const fleetSize = fleetSizeInput.value.trim();

        if (fleetSize === '') {
            fleetSizeError.textContent = 'Fleet size is required';
            fleetSizeError.style.color = 'red';

            return false;
        } else {
            fleetSizeError.textContent = '';
            return true;
        }
    }

    function validateDescription() {
        const descriptionInput = document.getElementById('description');
        const descriptionError = document.getElementById('descriptionError');
        const description = descriptionInput.value.trim();

        if (description === '') {
            descriptionError.textContent = 'Description id is required';
            descriptionError.style.color = 'red';

            return false;
        } else {
            descriptionError.textContent = '';
            return true;
        }
    }

    document.getElementById('name').addEventListener('blur', validateName);
    document.getElementById('founded').addEventListener('blur', validateFounded);
    document.getElementById('fleet_size').addEventListener('blur', validateFleetSize);
    document.getElementById('description').addEventListener('blur', validateDescription);

    flightForm.addEventListener('submit', function (event) {
        if (!validateName() &&
            !validateFounded() &&
            !validateFleetSize() &&
            !validateDescription()) {
            event.preventDefault();
        }
    });

    function updateSubmitButton() {
        if (flightForm.checkValidity()) {
            submitButton.disabled = false;
        } else {
            submitButton.disabled = true;
        }
    }

    flightForm.addEventListener('input', updateSubmitButton);
});

/*airport validation*/

// document.addEventListener('DOMContentLoaded', function () {
//     const flightForm = document.getElementById('flightForm');
//     const submitButton = document.getElementById('submitButton');
//     document.getElementById("submitButton").disabled = true;

//     function validateName() {
//         const nameInput = document.getElementById('name');
//         const nameError = document.getElementById('nameError');
//         const name = nameInput.value.trim();

//         if (name === '') {
//             nameError.textContent = 'Name is required';
//             nameError.style.color = 'red';

//             return false;
//         } else {
//             nameError.textContent = '';
//             return true;
//         }
//     }

//     function validateCountry() {
//         const countryInput = document.getElementById('country');
//         const countryError = document.getElementById('countryError');
//         const country = countryInput.value.trim();

//         if (country === '') {
//             countryError.textContent = 'Country is required';
//             countryError.style.color = 'red';

//             return false;
//         } else {
//             countryError.textContent = '';
//             return true;
//         }
//     }

//     function validateCity() {
//         const cityInput = document.getElementById('city');
//         const cityError = document.getElementById('cityError');
//         const city = cityInput.value.trim();

//         if (city === '') {
//             cityError.textContent = 'City is required';
//             cityError.style.color = 'red';

//             return false;
//         } else {
//             cityError.textContent = '';
//             return true;
//         }
//     }

//     function validateCode() {
//         const codeInput = document.getElementById('code');
//         const codeError = document.getElementById('codeError');
//         const code = codeInput.value.trim();

//         if (code === '') {
//             codeError.textContent = 'Code is required';
//             codeError.style.color = 'red';

//             return false;
//         } else {
//             codeError.textContent = '';
//             return true;
//         }
//     }

//     function validateRunwaysCount() {
//         const runwaysCountInput = document.getElementById('runways_count');
//         const runwaysCountError = document.getElementById('runwaysCountError');
//         const runwaysCount = runwaysCountInput.value.trim();

//         if (runwaysCount === '') {
//             runwaysCountError.textContent = 'Runways count is required';
//             runwaysCountError.style.color = 'red';

//             return false;
//         } else {
//             runwaysCountError.textContent = '';
//             return true;
//         }
//     }

//     function validateFounded() {
//         const foundedInput = document.getElementById('founded');
//         const foundedError = document.getElementById('foundedError');
//         const founded = foundedInput.value.trim();

//         if (founded === '') {
//             foundedError.textContent = 'Founded is required';
//             foundedError.style.color = 'red';

//             return false;
//         } else {
//             foundedError.textContent = '';
//             return true;
//         }
//     }

//     document.getElementById('name').addEventListener('blur', validateName);
//     document.getElementById('country').addEventListener('blur', validateCountry);
//     document.getElementById('city').addEventListener('blur', validateCity);
//     document.getElementById('code').addEventListener('blur', validateCode);
//     document.getElementById('runways_count').addEventListener('blur', validateRunwaysCount);
//     document.getElementById('founded').addEventListener('blur', validateFounded);


//     flightForm.addEventListener('submit', function (event) {
//         if (!validateName() &&
//             !validateCountry() &&
//             !validateCity() &&
//             !validateCode() &&
//             !validateRunwaysCount() &&
//             !validateFounded()) {
//             event.preventDefault();
//         }
//     });

//     function updateSubmitButton() {
//         if (flightForm.checkValidity()) {
//             submitButton.disabled = false;
//         } else {
//             submitButton.disabled = true;
//         }
//     }

//     flightForm.addEventListener('input', updateSubmitButton);
// });