﻿namespace Airlines.Business;

public interface IFindRoute
{
    List<Flight> SearchRoute();
}
