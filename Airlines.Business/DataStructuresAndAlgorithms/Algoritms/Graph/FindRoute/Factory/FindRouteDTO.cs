namespace Airlines.Business;

public class FindRouteDTO(Airport departure, Airport arrival)
{
    public Airport Departure { get; set; } = departure;
    public Airport Arrival { get; set; } = arrival;
}
