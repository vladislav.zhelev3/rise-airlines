﻿namespace Airlines.Business;

public static class FindRouteFactory
{
    internal static IFindRoute SearchStrategy(FindRouteDTO routeDTO, CommandDTO commandDTO, string command)
    {
        IFindRoute route = command.ToLower() switch
        {
            "cheap" => new FindCheapestWay(routeDTO, commandDTO),
            "short" => new FindShortestTimeWay(routeDTO, commandDTO),
            "stops" => new FindFewestStopsWay(routeDTO, commandDTO),
            _ => throw new InputCommandException("Not valid option!"),
        };
        return route;
    }
}
