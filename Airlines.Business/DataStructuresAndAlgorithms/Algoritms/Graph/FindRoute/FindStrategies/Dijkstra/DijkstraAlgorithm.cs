﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;

namespace Airlines.Business
{
    public static class DijkstraAlgorithm
    {
        public static List<Flight> DijkstraShortestPath(DirectedGraph<Airport, Flight> airportGraph, Airport sourceAirport, Airport destinationAirport, string? searchBy = null)
        {
            Dictionary<Airport, double> distances = [];
            Dictionary<Airport, Flight?> previousFlight = [];
            HashSet<Airport> visitedAirports = [];

            foreach (var vertex in airportGraph.GetVertices())
            {
                distances[vertex.Data] = double.MaxValue;
                previousFlight[vertex.Data] = null;
            }

            distances[sourceAirport] = 0;

            while (visitedAirports.Count < airportGraph.GetVertices().Count)
            {
                var currentAirport = GetClosestAirport(distances, visitedAirports);

                if (currentAirport == null)
                    break;

                _ = visitedAirports.Add(currentAirport);

                Vertex<Airport> currentVertex = airportGraph.GetVertex(currentAirport)!;

                var neighbors = airportGraph.GetNeighbors(currentVertex!);

                foreach (var flight in neighbors)
                {
                    var neighborAirport = flight.Destination.Data;
                    var alt = distances[currentAirport] + 1;
                    switch (searchBy)
                    {
                        case "price":
                            alt = distances[currentAirport] + flight.Weight.Price;
                            break;
                        case "duration":
                            alt = distances[currentAirport] + flight.Weight.DurationInMinutes;
                            break;
                        default:
                            break;
                    }

                    if (alt < distances[neighborAirport])
                    {
                        distances[neighborAirport] = alt;
                        previousFlight[neighborAirport] = flight.Weight;
                    }
                }
            }

            return ReconstructPath(previousFlight!, destinationAirport, airportGraph);
        }

        private static Airport GetClosestAirport(Dictionary<Airport, double> distances, HashSet<Airport> visitedAirports) => distances.Where(pair => !visitedAirports.Contains(pair.Key)).OrderBy(pair => pair.Value).FirstOrDefault().Key;

        private static List<Flight> ReconstructPath(Dictionary<Airport, Flight> previousFlight, Airport destinationAirport, DirectedGraph<Airport, Flight> airportGraph)
        {
            List<Flight> path = [];

            Flight currentFlight = previousFlight[destinationAirport];
            while (currentFlight != null)
            {
                path.Add(currentFlight);
                destinationAirport = GetAirportByIdentifier(currentFlight.DepartureAirport.Identifier, airportGraph);
                currentFlight = previousFlight[destinationAirport];
            }

            path.Reverse();
            return path;
        }

        private static Airport GetAirportByIdentifier(string identifier, DirectedGraph<Airport, Flight> airportGraph)
        {

            foreach (var vertex in airportGraph.AdjacencyList.Keys)
            {
                if (vertex.Data.Identifier == identifier)
                {
                    return vertex.Data;
                }
            }
            throw new ArgumentException("Airport with the given identifier not found.");
        }
    }
}
