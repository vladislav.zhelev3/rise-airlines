﻿namespace Airlines.Business;

public class FindShortestTimeWay(FindRouteDTO routeDTO, CommandDTO commandDTO) : IFindRoute
{
    private readonly FindRouteDTO _routeDTO = routeDTO;
    private readonly CommandDTO _commandDTO = commandDTO;
    public List<Flight> SearchRoute()
    {
        List<Flight> route;
        string searchBy = "duration";

        route = DijkstraAlgorithm.DijkstraShortestPath(_commandDTO.AirportGraph, _routeDTO.Departure, _routeDTO.Arrival, searchBy);

        return route;
    }
}