﻿namespace Airlines.Business;

public static class PrintAirportGraph
{

    internal static void PrintAirportGraphDetails(this DirectedGraph<Airport, Flight> airportGraph)
    {
        foreach (var kvp in airportGraph.AdjacencyList)
        {
            ConsoleLoggerBusiness.Log(kvp.Key.Data.Identifier + " -> ");
            foreach (Edge<Airport, Flight> edge in kvp.Value)
            {
                ConsoleLoggerBusiness.Log(edge.Destination.Data.Identifier + " ( Id: " + edge.Weight.Identifier + "; ");
                ConsoleLoggerBusiness.Log("Price: " + edge.Weight.Price + "; Duration: " + edge.Weight.DurationInMinutes + " ); ");
            }
            ConsoleLoggerBusiness.LogLine();
        }
    }
}
