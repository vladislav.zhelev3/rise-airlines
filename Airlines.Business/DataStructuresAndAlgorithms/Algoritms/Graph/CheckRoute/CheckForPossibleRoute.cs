﻿namespace Airlines.Business;

public static class CheckForPossibleRoute
{
    internal static bool CheckRoute(this DirectedGraph<Airport, Flight> airportGraph, string departureAirportIdentifier, string arrivalAirportIdentifier)
    {
        bool hasRoute = false;

        Vertex<Airport>? departureVertex = airportGraph.GetVertices().Find(vertex => vertex.Data.Identifier == departureAirportIdentifier);
        Vertex<Airport>? arrivalVertex = airportGraph.GetVertices().Find(vertex => vertex.Data.Identifier == arrivalAirportIdentifier);

        hasRoute = departureVertex != null && arrivalVertex != null
            ? airportGraph.HasRoute(departureVertex, arrivalVertex)
            : throw new InputCommandException("Departure or arrival airport not found.");

        return hasRoute;
    }
}
