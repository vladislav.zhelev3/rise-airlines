﻿namespace Airlines.Business;

public static class GetVertexAirport
{
    internal static Vertex<Airport>? GetVertex(this DirectedGraph<Airport, Flight> airportGraph, Airport airport)
    {
        foreach (var kvp in airportGraph.AdjacencyList)
        {
            if (kvp.Key.Data.Identifier == airport.Identifier)
            {
                return kvp.Key;
            }
        }
        return null;
    }
}


