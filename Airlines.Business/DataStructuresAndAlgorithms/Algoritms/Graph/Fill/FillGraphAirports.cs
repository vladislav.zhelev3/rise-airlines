﻿namespace Airlines.Business;

public static class FillGraphAirports
{
    internal static void FillGraphWithAirports(this DirectedGraph<Airport, Flight> airportGraph, CommandDTO commandDto)
    {
        foreach (var airport in commandDto.Airports)
        {
            Vertex<Airport> vertex = new Vertex<Airport>(airport);
            airportGraph.AddVertex(vertex);
        }

        foreach (var kvp in commandDto.CsvFlights)
        {
            string flightKey = kvp.Key;
            string flightValue = kvp.Value;
            string[] flightInfo = flightValue.Split(',');

            string departureAirportIdentifier = flightInfo[0];
            string arrivalAirportIdentifier = flightInfo[1];

            Vertex<Airport>? departureVertex = airportGraph.GetVertices().Find(vertex => vertex.Data.Identifier == departureAirportIdentifier);
            Vertex<Airport>? arrivalVertex = airportGraph.GetVertices().Find(vertex => vertex.Data.Identifier == arrivalAirportIdentifier);

            if (departureVertex != null && arrivalVertex != null)
            {
                airportGraph.AddEdge(departureVertex, arrivalVertex, new Flight(flightKey, GetAirportObjectByString.GetAirport(departureAirportIdentifier, commandDto.Airports)!, GetAirportObjectByString.GetAirport(arrivalAirportIdentifier, commandDto.Airports)!, AircraftFactory.ParseAircraft(flightInfo[2], commandDto.CsvAircraft)!, flightInfo[3], flightInfo[4]));
            }
            else
            {
                throw new NullOnNotNullException("Departure or arrival vertex not found.");
            }
        }
    }
}
