﻿namespace Airlines.Business;

public static class FillAirportsTree
{
    internal static void FillTreeWithAirports(this Tree<string> tree, Dictionary<string, string> csvFlights)
    {
        foreach (var item in csvFlights.Values)
        {

            tree.AddChild(item.Split(",")[0], item.Split(",")[1]);

        }
    }
}
