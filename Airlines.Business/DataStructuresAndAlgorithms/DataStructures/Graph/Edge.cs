﻿namespace Airlines.Business;

public class Edge<T, U>(Vertex<T> source, Vertex<T> destination, U weight)
{
    public Vertex<T> Source { get; private set; } = source;
    public Vertex<T> Destination { get; private set; } = destination;
    public U Weight { get; private set; } = weight;
}
