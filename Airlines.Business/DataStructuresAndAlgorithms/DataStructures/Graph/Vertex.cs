﻿
namespace Airlines.Business;

public class Vertex<T>(T data)
{
    public T Data { get; private set; } = data;
}

