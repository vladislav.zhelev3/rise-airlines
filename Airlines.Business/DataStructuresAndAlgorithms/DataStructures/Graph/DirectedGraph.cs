﻿namespace Airlines.Business;

public class DirectedGraph<T, U>
{
    internal DirectedGraph() => AdjacencyList = [];

    internal List<Vertex<T>> GetVertices()
    {
        List<Vertex<T>> vertices = [.. AdjacencyList.Keys];
        return vertices;
    }

    internal List<Edge<T, U>> GetNeighbors(Vertex<T> currentVertex)
    {
        if (!AdjacencyList.TryGetValue(currentVertex, out var edges))
        {
            throw new ArgumentException("Vertex not found in the graph.");
        }
        return edges;
    }

    internal void AddVertex(Vertex<T> vertex)
    {
        if (!AdjacencyList.ContainsKey(vertex))
        {
            AdjacencyList[vertex] = [];
        }
    }

    internal void RemoveVertex(Vertex<T> vertex)
    {
        _ = AdjacencyList.Remove(vertex);

        foreach (var kvp in AdjacencyList)
        {
            _ = kvp.Value.RemoveAll(edge => edge.Destination == vertex);
        }
    }

    internal void AddEdge(Vertex<T> source, Vertex<T> destination, U weight)
    {
        if (!AdjacencyList.TryGetValue(source, out var value) || !AdjacencyList.ContainsKey(destination))
        {
            throw new ArgumentException("Source or destination vertex not found.");
        }

        value.Add(new Edge<T, U>(source, destination, weight));
    }

    internal bool HasRoute(Vertex<T> source, Vertex<T> destination)
    {
        var visited = new HashSet<Vertex<T>>();

        return DFS(source, destination, visited);
    }

    private bool DFS(Vertex<T> current, Vertex<T> destination, HashSet<Vertex<T>> visited)
    {
        if (current == destination)
            return true;

        _ = visited.Add(current);

        foreach (var neighbor in AdjacencyList[current])
        {
            if (!visited.Contains(neighbor.Destination))
            {
                if (DFS(neighbor.Destination, destination, visited))
                    return true;
            }
        }

        return false;
    }

    internal void BFS(Vertex<T> startVertex)
    {
        HashSet<Vertex<T>> visited = [];
        Queue<Vertex<T>> queue = new Queue<Vertex<T>>();

        _ = visited.Add(startVertex);
        queue.Enqueue(startVertex);

        while (queue.Count != 0)
        {
            Vertex<T> currentVertex = queue.Dequeue();
            ConsoleLoggerBusiness.Log(currentVertex.Data + " ");

            if (AdjacencyList.TryGetValue(currentVertex, out var value))
            {
                foreach (Edge<T, U> edge in value)
                {
                    if (!visited.Contains(edge.Destination))
                    {
                        _ = visited.Add(edge.Destination);
                        queue.Enqueue(edge.Destination);
                    }
                }
            }
        }
    }

    public Dictionary<Vertex<T>, List<Edge<T, U>>> AdjacencyList { get; }
}