﻿namespace Airlines.Business;


public class Tree<T>(T rootData)
{
    public TreeNode<T> Root { get; set; } = new TreeNode<T>(rootData);

    internal void Traverse(Action<T> action) => Tree<T>.Traverse(Root, action);

    private static void Traverse(TreeNode<T> node, Action<T> action)
    {
        action(node.Data);
        foreach (var child in node.Children)
        {
            Tree<T>.Traverse(child, action);
        }
    }

    internal void AddChild(T parentData, T childData)
    {
        var parentNode = Tree<T>.FindNode(Root, parentData);
        if (parentNode != null)
        {
            parentNode.Children.Add(new TreeNode<T>(childData));
        }
        else
        {
            throw new InvalidOperationException("Parent node not found.");
        }
    }

    private static TreeNode<T>? FindNode(TreeNode<T> currentNode, T data)
    {
        if (EqualityComparer<T>.Default.Equals(currentNode.Data, data))
        {
            return currentNode;
        }

        foreach (var child in currentNode.Children)
        {
            var foundNode = Tree<T>.FindNode(child, data);
            if (foundNode != null)
            {
                return foundNode;
            }
        }

        return null;
    }

    internal static void PrintTree(TreeNode<T> tree, string indent, bool last)
    {
        ConsoleLoggerBusiness.LogLine(indent + "+- " + tree.Data);
        indent += last ? "   " : "|  ";

        for (int i = 0; i < tree.Children.Count; i++)
        {
            Tree<T>.PrintTree(tree.Children[i], indent, i == tree.Children.Count - 1);
        }
    }

    internal List<T>? CanReach(T source, T destination)
    {
        var sourceNode = Tree<T>.FindNode(Root, source);
        var destinationNode = Tree<T>.FindNode(Root, destination);

        if (sourceNode == null || destinationNode == null || sourceNode == destinationNode)
        {
            return null;
        }

        var route = new List<T>();
        if (Tree<T>.CanReachDFS(sourceNode, destinationNode, route))
        {
            return route;
        }

        return null;
    }

    private static bool CanReachDFS(TreeNode<T> currentNode, TreeNode<T> destinationNode, List<T> route)
    {
        route.Add(currentNode.Data);

        if (EqualityComparer<T>.Default.Equals(currentNode.Data, destinationNode.Data))
        {
            return true;
        }

        foreach (var child in currentNode.Children)
        {
            if (Tree<T>.CanReachDFS(child, destinationNode, route))
            {
                return true;
            }
        }

        route.RemoveAt(route.Count - 1);
        return false;
    }

    internal bool Remove(T dataToRemove)
    {
        if (EqualityComparer<T>.Default.Equals(Root.Data, dataToRemove))
        {
            return false;
        }

        return Tree<T>.Remove(Root, dataToRemove);
    }

    private static bool Remove(TreeNode<T> currentNode, T dataToRemove)
    {
        foreach (var child in currentNode.Children)
        {
            if (EqualityComparer<T>.Default.Equals(child.Data, dataToRemove))
            {
                _ = currentNode.Children.Remove(child);
                return true;
            }

            if (Tree<T>.Remove(child, dataToRemove))
            {
                return true;
            }
        }

        return false;
    }
}