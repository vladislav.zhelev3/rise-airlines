﻿namespace Airlines.Business;

public class TreeNode<T>(T data)
{
    public T Data { get; set; } = data;
    public List<TreeNode<T>> Children { get; } = [];
}
