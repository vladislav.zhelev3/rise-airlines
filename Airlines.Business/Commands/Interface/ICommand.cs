namespace Airlines.Business;

public interface ICommand
{
    void Execute();
}
