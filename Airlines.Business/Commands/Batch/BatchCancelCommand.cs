﻿namespace Airlines.Business;

public class BatchCancelCommand(Queue<ICommand> batchCommandQueue) : ICommand
{
    private readonly Queue<ICommand> _batchCommandQueue = batchCommandQueue;

    public void Execute()
    {
        _batchCommandQueue.Clear();
        ConsoleLoggerBusiness.LogLine("Batch queue was cleared");
    }
}