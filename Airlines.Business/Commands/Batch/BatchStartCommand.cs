﻿namespace Airlines.Business;

public class BatchStartCommand(CommandDTO commandDto) : ICommand
{
    private readonly CommandDTO _commandDto = commandDto;
    private readonly HashSet<string> _invalidCommands = [];



    private void AddInvalidCommands()
    {
        _ = _invalidCommands.Add("batch start");
        _ = _invalidCommands.Add("batch run");
        _ = _invalidCommands.Add("batch cancel");
    }

    public void Execute()
    {
        string command;
        AddInvalidCommands();

        ConsoleLoggerBusiness.LogLine("Write ( stop ) to stop input.");
        do
        {
            ConsoleLoggerBusiness.Log("> ");
            command = ConsoleInputBusiness.Input();
            if (_invalidCommands.Contains(command.ToLower()))
                ConsoleLoggerBusiness.LogLine("Invalid command!");
            else if (!command.Equals("stop", StringComparison.CurrentCultureIgnoreCase))
                _commandDto.BatchCommandQueue.Enqueue(CommandFactory.Parse(_commandDto, command)!);
        }
        while (!command.Equals("stop", StringComparison.CurrentCultureIgnoreCase));
    }
}