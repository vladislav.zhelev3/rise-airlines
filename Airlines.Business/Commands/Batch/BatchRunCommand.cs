﻿namespace Airlines.Business;
public class BatchRunCommand(Queue<ICommand> batchCommandQueue) : ICommand
{
    private readonly Queue<ICommand> _batchCommandQueue = batchCommandQueue;

    public void Execute()
    {
        ConsoleLoggerBusiness.LogLine("Batch queue running.....");
        while (_batchCommandQueue.Count != 0)
        {
            try
            {
                _batchCommandQueue.Dequeue().Execute();
            }
            catch (InputCommandException inputException)
            {
                ConsoleLoggerBusiness.LogLine(inputException.Message);
            }
        }
        ConsoleLoggerBusiness.LogLine("Batch queue is empty!");
    }
}