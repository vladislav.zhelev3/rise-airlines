﻿namespace Airlines.Business;

public class ReserveCargoCommand(LinkedList<Flight> flights, string command) : ICommand
{
    private readonly LinkedList<Flight> _flights = flights;
    private readonly string _command = command;

    public void Execute()
    {
        if (_flights.Count != 0)
        {
            string flightIdentifier = _command.Split(' ')[2];
            foreach (var flight in _flights)
            {
                if (flight.Identifier.Contains(flightIdentifier))
                {
                    ConsoleLoggerBusiness.LogLine("Done!");
                    return;
                }
            }
            throw new InputCommandException("Flight not found!");
        }
        throw new InputCommandException("Flights not found!");
    }
}