﻿namespace Airlines.Business;

public class ReserveTicketCommand(LinkedList<Flight> flights, string command) : ICommand
{
    private readonly LinkedList<Flight> _flights = flights;
    private readonly string _command = command;

    public void Execute()
    {
        if (_flights.Count != 0)
        {
            string flightIdentifier = _command.Split(' ')[2];
            double smallBaggage = double.Parse(_command.Split(' ')[4]);
            double bigBaggage = double.Parse(_command.Split(' ')[5]);

            if (bigBaggage > 30 || smallBaggage > 15)
            {
                throw new InputCommandException("Small baggage should be under 15kg and Big baggage should be under 30kg");
            }

            foreach (var flight in _flights)
            {
                if (flight.Identifier.Contains(flightIdentifier))
                {
                    ConsoleLoggerBusiness.LogLine("Done!");
                    return;
                }
            }
            throw new InputCommandException("Flight not found!");
        }
        throw new InputCommandException("Flights not found!");
    }
}