﻿namespace Airlines.Business;

public class TreeRouteCommand(Tree<string> tree, string command) : ICommand
{
    private readonly Tree<string> _tree = tree;
    private readonly string _command = command;
    public void Execute()
    {
        string firstAirport = _command.Split(' ')[2];
        string lastAirport = _command.Split(' ')[3];

        List<string>? possibleRoute = _tree.CanReach(firstAirport, lastAirport);
        if (possibleRoute is not null)
            ConsoleLoggerBusiness.LogLine(string.Join(" - ", possibleRoute));
        else
            throw new InputCommandException("This route is not possible!");
    }
}
