﻿namespace Airlines.Business;

public class TreePrintCommand(Tree<string> tree) : ICommand
{
    private readonly Tree<string> _tree = tree;
    public void Execute() => Tree<string>.PrintTree(_tree.Root, "", true);
}
