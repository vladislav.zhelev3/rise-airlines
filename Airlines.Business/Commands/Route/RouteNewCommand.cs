﻿namespace Airlines.Business;

public class RouteNewCommand(LinkedList<Flight> flights) : ICommand
{
    private readonly LinkedList<Flight> _flights = flights;
    public void Execute()
    {
        _flights.Clear();
        ConsoleLoggerBusiness.LogLine("You clear all flights.");
    }
}