﻿namespace Airlines.Business;

public class RouteAddCommand(CommandDTO commandDto, string command) : ICommand
{
    private readonly CommandDTO _commandDto = commandDto;
    private readonly string _command = command;

    public void Execute()
    {
        try
        {
            string[] commandParams = _command.Split(' ');
            if (_commandDto.CsvFlights.TryGetValue(commandParams[2], out var value))
            {
                if (_commandDto.Flights.Count == 0)
                {
                    string flightIdentity = commandParams[2];
                    string[] flightProps = value.Split(',');

                    var newFlight = new Flight(flightIdentity, GetAirportObjectByString.GetAirport(flightProps[0], _commandDto.Airports)!, GetAirportObjectByString.GetAirport(flightProps[1], _commandDto.Airports)!, AircraftFactory.ParseAircraft(flightProps[2], _commandDto.CsvAircraft)!, flightProps[3], flightProps[4]);
                    _ = _commandDto.Flights.AddLast(newFlight);
                    ConsoleLoggerBusiness.LogLine("You added a new flight.");
                    return;
                }

                foreach (var flightItem in _commandDto.Flights)
                {
                    if (flightItem.Identifier == commandParams[2])
                    {
                        throw new InputCommandException("You can't repeat flight!");
                    }
                }

                var lastFlight = _commandDto.Flights.Last!.Value;
                if (lastFlight.ArrivalAirport.Equals(value.Split(',')[0]))
                {
                    string flightIdentity = commandParams[2];
                    string[] flightProps = value.Split(',');

                    var newFlight = new Flight(flightIdentity, GetAirportObjectByString.GetAirport(flightProps[0], _commandDto.Airports)!, GetAirportObjectByString.GetAirport(flightProps[1], _commandDto.Airports)!, AircraftFactory.ParseAircraft(flightProps[2], _commandDto.CsvAircraft)!, flightProps[3], flightProps[4]);
                    _ = _commandDto.Flights.AddLast(newFlight);
                    ConsoleLoggerBusiness.LogLine("You added a new flight.");
                    return;
                }
                throw new InputCommandException("Flight should departure from last arival airport!");
            }
            throw new InputCommandException("Flight not found!");
        }
        catch (AircraftValidationException flightException)
        {
            throw new InputCommandException(flightException.Message);
        }
    }
}