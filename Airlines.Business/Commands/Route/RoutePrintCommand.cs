﻿namespace Airlines.Business;

public class RoutePrintCommand(LinkedList<Flight> flights) : ICommand
{
    private readonly LinkedList<Flight> _flights = flights;
    public void Execute()
    {

        if (_flights.Count == 0)
        {
            throw new InputCommandException("Flights not found!");
        }

        string flightStringResult = "";
        foreach (var flightItem in _flights)
        {
            flightStringResult += flightItem.Identifier + '\t' + flightItem.DepartureAirport + '\t' + flightItem.ArrivalAirport + '\t' + flightItem.Aircraft.Model + '\t' + flightItem.Price + '\t' + flightItem.DurationInMinutes + '\n';
        }
        ConsoleLoggerBusiness.LogLine(flightStringResult);
    }
}