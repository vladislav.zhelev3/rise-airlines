﻿namespace Airlines.Business;

public class RouteRemoveCommand(LinkedList<Flight> flights) : ICommand
{
    private readonly LinkedList<Flight> _flights = flights;
    public void Execute()
    {

        if (_flights.Count == 0)
        {
            throw new InputCommandException("Flights to remove not found!");
        }

        _flights.RemoveLast();
        ConsoleLoggerBusiness.LogLine("You removed last flight.");
    }
}