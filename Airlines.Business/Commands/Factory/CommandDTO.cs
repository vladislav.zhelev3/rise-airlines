﻿namespace Airlines.Business;

public class CommandDTO(List<Airport> airports, HashSet<string> airlineName, Dictionary<string, string> csvFlights, LinkedList<Flight> flights, Queue<ICommand> batchCommandQueue, List<string> csvAircraft, Tree<string> tree, DirectedGraph<Airport, Flight> airportGraph)
{
    public List<Airport> Airports { get; set; } = airports;
    public HashSet<string> AirlineName { get; set; } = airlineName;
    public Dictionary<string, string> CsvFlights { get; set; } = csvFlights;
    public LinkedList<Flight> Flights { get; set; } = flights;
    public Queue<ICommand> BatchCommandQueue { get; set; } = batchCommandQueue;
    public List<string> CsvAircraft { get; set; } = csvAircraft;
    public Tree<string> Tree { get; set; } = tree;
    public DirectedGraph<Airport, Flight> AirportGraph { get; set; } = airportGraph;
}
