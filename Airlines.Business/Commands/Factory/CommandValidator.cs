﻿namespace Airlines.Business;

public static class CommandValidator
{
    internal static bool IsValidExistCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command) && command.Split(' ').Length == 2)
        {
            string[] commandParams = command.Split(' ');

            if (!string.Equals(commandParams[0], "exist", StringComparison.OrdinalIgnoreCase))
                return false;
            else
                return true;
        }
        return false;
    }

    internal static bool IsValidListCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (!string.Equals(commandParams[0], "list", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[^1], "city", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(commandParams[^1], "country", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidNewRouteCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 2)
                return false;

            if (!string.Equals(commandParams[0], "route", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "new", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidAddRouteCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 3)
                return false;

            if (!string.Equals(commandParams[0], "route", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "add", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidRemoveRouteCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 2)
                return false;

            if (!string.Equals(commandParams[0], "route", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "remove", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidPrintRouteCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 2)
                return false;

            if (!string.Equals(commandParams[0], "route", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "print", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidReserveCargoCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 5)
                return false;

            if (!string.Equals(commandParams[0], "reserve", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "cargo", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!commandParams[2].All(char.IsLetterOrDigit))
                return false;

            bool cargoWeight = double.TryParse(command.Split(' ')[3], out var a);
            bool cargoVolume = double.TryParse(command.Split(' ')[4], out var b);

            return cargoVolume && cargoVolume!;
        }
        return false;
    }


    internal static bool IsValidReserveTicketCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 6)
                return false;

            if (!string.Equals(commandParams[0], "reserve", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "ticket", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!commandParams[2].All(char.IsLetterOrDigit))
                return false;

            bool seats = int.TryParse(command.Split(' ')[3], out var a);
            bool trySmallBaggage = double.TryParse(command.Split(' ')[4], out var b);
            bool tryBigBaggage = double.TryParse(command.Split(' ')[5], out var c);

            return seats && trySmallBaggage && tryBigBaggage!;
        }
        return false;
    }

    internal static bool IsValidBatchStartCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 2)
                return false;

            if (!string.Equals(commandParams[0], "batch", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "start", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidBatchRunCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 2)
                return false;

            if (!string.Equals(commandParams[0], "batch", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "run", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidBatchCancelCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 2)
                return false;

            if (!string.Equals(commandParams[0], "batch", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "cancel", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidTreePrintCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 2)
                return false;

            if (!string.Equals(commandParams[0], "tree", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "print", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidTreeRouteCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 4)
                return false;

            if (!string.Equals(commandParams[0], "tree", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "route", StringComparison.OrdinalIgnoreCase))
                return false;

            string departureAirport = commandParams[2];
            string arivalAirport = commandParams[3];

            if (departureAirport.Length != 3 || arivalAirport.Length != 3)
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidGraphRoutePrintCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 2)
                return false;

            if (!string.Equals(commandParams[0], "groute", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "print", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidGraphRouteCheckCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 4)
                return false;

            if (!string.Equals(commandParams[0], "groute", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "check", StringComparison.OrdinalIgnoreCase))
                return false;

            string departureAirport = commandParams[2];
            string arivalAirport = commandParams[3];

            if (departureAirport.Length != 3 || arivalAirport.Length != 3)
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidGraphRouteSearchCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            string[] commandParams = command.Split(' ');

            if (commandParams.Length != 5)
                return false;

            if (!string.Equals(commandParams[0], "groute", StringComparison.OrdinalIgnoreCase))
                return false;

            if (!string.Equals(commandParams[1], "search", StringComparison.OrdinalIgnoreCase))
                return false;

            string departureAirport = commandParams[2];
            string arivalAirport = commandParams[3];

            if (departureAirport.Length != 3 || arivalAirport.Length != 3)
                return false;

            string strategy = commandParams[4];

            switch (strategy.ToLower())
            {
                case "cheap":
                    break;
                case "short":
                    break;
                case "stops":
                    break;
                default:
                    return false;
            }

            return true;
        }
        return false;
    }

    internal static bool IsValidHelpCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            if (!string.Equals(command, "help", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }

    internal static bool IsValidExitCommand(string command)
    {
        if (!string.IsNullOrWhiteSpace(command))
        {
            if (!string.Equals(command, "exit", StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
        return false;
    }
}
