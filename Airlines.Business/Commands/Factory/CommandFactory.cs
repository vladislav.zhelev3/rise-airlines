namespace Airlines.Business;

public static class CommandFactory
{
#pragma warning disable IDE0045
    internal static ICommand Parse(CommandDTO commandDto, string command)
    {
        ICommand commandObject;

        if (CommandValidator.IsValidExistCommand(command))
        {
            commandObject = new ExistCommand(commandDto.AirlineName, string.Join(" ", command.Split().Skip(1)));
        }
        else if (CommandValidator.IsValidListCommand(command))
        {
            commandObject = new ListCommand(commandDto.Airports, command);
        }
        else if (CommandValidator.IsValidNewRouteCommand(command))
        {
            commandObject = new RouteNewCommand(commandDto.Flights);
        }
        else if (CommandValidator.IsValidAddRouteCommand(command))
        {
            commandObject = new RouteAddCommand(commandDto, command);
        }
        else if (CommandValidator.IsValidRemoveRouteCommand(command))
        {
            commandObject = new RouteRemoveCommand(commandDto.Flights);
        }
        else if (CommandValidator.IsValidPrintRouteCommand(command))
        {
            commandObject = new RoutePrintCommand(commandDto.Flights);
        }
        else if (CommandValidator.IsValidReserveCargoCommand(command))
        {
            commandObject = new ReserveCargoCommand(commandDto.Flights, command);
        }
        else if (CommandValidator.IsValidReserveTicketCommand(command))
        {
            commandObject = new ReserveTicketCommand(commandDto.Flights, command);
        }
        else if (CommandValidator.IsValidBatchRunCommand(command))
        {
            commandObject = new BatchRunCommand(commandDto.BatchCommandQueue);
        }
        else if (CommandValidator.IsValidBatchStartCommand(command))
        {
            commandObject = new BatchStartCommand(commandDto);
        }
        else if (CommandValidator.IsValidBatchCancelCommand(command))
        {
            commandObject = new BatchCancelCommand(commandDto.BatchCommandQueue);
        }
        else if (CommandValidator.IsValidTreePrintCommand(command))
        {
            commandObject = new TreePrintCommand(commandDto.Tree);
        }
        else if (CommandValidator.IsValidTreeRouteCommand(command))
        {
            commandObject = new TreeRouteCommand(commandDto.Tree, command);
        }
        else if (CommandValidator.IsValidHelpCommand(command))
        {
            commandObject = new HelpCommand();
        }
        else if (CommandValidator.IsValidGraphRoutePrintCommand(command))
        {
            commandObject = new PrintGraphCommand(commandDto.AirportGraph);
        }
        else if (CommandValidator.IsValidGraphRouteCheckCommand(command))
        {
            commandObject = new CheckForPossibleRouteCommand(commandDto, command);
        }
        else if (CommandValidator.IsValidGraphRouteSearchCommand(command))
        {
            commandObject = new SearchRouteCommand(commandDto, command);
        }
        else
        {
            commandObject = CommandValidator.IsValidExitCommand(command) ? new ExitCommand() : new NotFoundCommand();
        }


        return commandObject;
    }
}