﻿namespace Airlines.Business;

public class ExistCommand(HashSet<string> airlineNames, string target) : ICommand
{
    private readonly HashSet<string> _airlineNames = airlineNames;
    private readonly string _target = target;

    public void Execute() => ConsoleLoggerBusiness.LogLine(_airlineNames.Contains(_target).ToString());
}