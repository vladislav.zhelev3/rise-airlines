﻿namespace Airlines.Business;

public class ListCommand(List<Airport> airports, string command) : ICommand
{
    private readonly List<Airport> _airports = airports;
    private readonly string _command = command;
    public void Execute()
    {
        string[] commandParams = _command.Split(' ');
        string nameResults = "";
        if (commandParams[^1].Equals("city", StringComparison.CurrentCultureIgnoreCase))
        {
            for (int i = 0; i < _airports.Count; i++)
            {
                if (_airports[i].City.Equals(string.Join(' ', _command.Split(' ').Take(_command.Split(' ').Length - 1).ToArray().Skip(1)), StringComparison.CurrentCultureIgnoreCase))
                    nameResults += _airports[i].Name + '\n';
            }
        }
        else
        {
            for (int i = 0; i < _airports.Count; i++)
            {
                if (_airports[i].Country.Equals(string.Join(' ', _command.Split(' ').Take(_command.Split(' ').Length - 1).ToArray().Skip(1)), StringComparison.CurrentCultureIgnoreCase))
                    nameResults += _airports[i].Name + '\n';
            }
        }
        ConsoleLoggerBusiness.LogLine(nameResults);
    }
}