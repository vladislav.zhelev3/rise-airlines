﻿namespace Airlines.Business;

public class CheckForPossibleRouteCommand(CommandDTO commandDto, string command) : ICommand
{
    private readonly CommandDTO _commandDto = commandDto;
    private readonly string _command = command;
    public void Execute() => ConsoleLoggerBusiness.LogLine(_commandDto.AirportGraph.CheckRoute(_command.Split(" ")[2], _command.Split(" ")[3]).ToString());
}
