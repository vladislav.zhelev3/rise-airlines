﻿namespace Airlines.Business;

public class PrintGraphCommand(DirectedGraph<Airport, Flight> airportGraph) : ICommand
{
    private readonly DirectedGraph<Airport, Flight> _airportGraph = airportGraph;
    public void Execute() => _airportGraph.PrintAirportGraphDetails();
}