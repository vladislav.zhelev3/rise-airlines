﻿namespace Airlines.Business;

public class SearchRouteCommand(CommandDTO commandDto, string command) : ICommand
{
    private readonly CommandDTO _commandDto = commandDto;
    public readonly string _command = command;

    private IFindRoute? _searchBestRoute;
    public void Execute()
    {
        string departureAirportIdentifier = _command.Split(" ")[2];
        string arrivalAirportIdentifier = _command.Split(" ")[3];

        Airport? departureAirport = GetAirportObjectByString.GetAirport(departureAirportIdentifier, _commandDto.Airports);
        Airport? arrivalAirport = GetAirportObjectByString.GetAirport(arrivalAirportIdentifier, _commandDto.Airports);

        if (departureAirport != null && arrivalAirport != null)
        {
            FindRouteDTO routeDTO = new FindRouteDTO(departureAirport, arrivalAirport);

            List<Flight> route;
            _searchBestRoute = FindRouteFactory.SearchStrategy(routeDTO, _commandDto, _command.Split(" ")[4])!;
            route = _searchBestRoute.SearchRoute();

            if (route.Count is 0)
            {
                throw new InputCommandException("No, possible route.");
            }

            ConsoleLoggerBusiness.Log(route[0].DepartureAirport.Identifier);
            foreach (var flight in route)
            {
                ConsoleLoggerBusiness.Log(" - " + flight.ArrivalAirport.Identifier);
            }
            ConsoleLoggerBusiness.LogLine();
        }
        else
            throw new InputCommandException("Airports are null.");
    }
}