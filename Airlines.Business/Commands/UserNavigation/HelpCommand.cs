﻿namespace Airlines.Business;
public class HelpCommand : ICommand
{
    public void Execute()
    {
        string nORMAL = Console.IsOutputRedirected ? "" : "\x1b[39m";
        string gREEN = Console.IsOutputRedirected ? "" : "\x1b[92m";

        string help = $"( {gREEN}exit{nORMAL} ) - Stop program." + '\n'
                        + $"( {gREEN}help{nORMAL} ) - See all commands." + '\n'
                        + $"( {gREEN}exist <airline name>{nORMAL} ) - The command prints true if an airline with a matching name is existing in our project, or false if not." + '\n'
                        + $"( {gREEN}list <input data> <from>{nORMAL} ) - <input data>: Required, the name of a city or a country. <from>: Required, specify 'City' or 'Country' corresponding to the input data." + '\n'
                        + $"( {gREEN}route new{nORMAL} ) - Initializes a new flight route." + '\n'
                        + $"( {gREEN}route add <Flight Identifier>{nORMAL} ) - Adds a flight to the end of the route using its mandatory unique identifier, ensuring its logical connection to the route." + '\n'
                        + $"( {gREEN}route remove{nORMAL} ) - Removes the last flight from the route." + '\n'
                        + $"( {gREEN}route print{nORMAL} ) - Prints all flights." + '\n'
                        + $"( {gREEN}reserve cargo <Flight Identifier> <Cargo Weight> <Cargo Volume>{nORMAL} ) - <Flight Identifier>: Mandatory, the identifier of the flight for cargo reservation. <Cargo Weight>: Mandatory, the weight of the cargo in kilograms. <Cargo Volume>: Mandatory, volume of the cargo in cubic meters." + '\n'
                        + $"( {gREEN}reserve ticket <Flight Identifier> <Seats> <Small Baggage Count> <Large Baggage Count>{nORMAL} ) - <Flight Identifier>: Mandatory, the identifier of the flight for ticket reservation.<Seats>: Mandatory, number of seats to book. <Small Baggage Count>: Mandatory, the number of small baggages. <Large Baggage Count>: Mandatory, the number of large baggages." + '\n'
                        + $"( {gREEN}batch start{nORMAL} ) - Activates batch mode, where new commands are added to a batch queue instead of being executed immediately" + '\n'
                        + $"( {gREEN}batch run{nORMAL} ) - Executes all commands in the batch queue." + '\n'
                        + $"( {gREEN}batch cancel{nORMAL} ) - Clears the batch queue." + '\n'
                        + $"( {gREEN}tree print{nORMAL} ) - Print airport tree." + '\n'
                        + $"( {gREEN}tree route{nORMAL} ) - Check for possible route through airport tree." + '\n'
                        + $"( {gREEN}groute check <Start Airport> <End Airport>{nORMAL} ) - Check for connectivity between two airports." + '\n'
                        + $"( {gREEN}groute search <Start Airport> <End Airport> <Strategy>{nORMAL} ) - <Strategy> options: 'cheap', 'short', 'stops'. Search for shortest way between two airports based on strategy." + '\n'
                        + $"( {gREEN}groute print{nORMAL} ) - Print airport graph." + '\n';
        ConsoleLoggerBusiness.LogLine(help);
    }
}