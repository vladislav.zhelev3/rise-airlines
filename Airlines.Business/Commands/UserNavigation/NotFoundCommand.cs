﻿namespace Airlines.Business;

public class NotFoundCommand : ICommand
{
    public void Execute() => throw new InputCommandException("Error: Command not found!");
}
