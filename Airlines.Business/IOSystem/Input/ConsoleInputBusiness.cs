﻿namespace Airlines.Business;

public class ConsoleInputBusiness : IBusinessInput
{
    public static string Input() => Console.ReadLine()!;
}
