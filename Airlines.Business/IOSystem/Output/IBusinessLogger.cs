﻿namespace Airlines.Business;

public interface IBusinessLogger
{
    static abstract void Log(string output);
    static abstract void LogLine(string output);
}
