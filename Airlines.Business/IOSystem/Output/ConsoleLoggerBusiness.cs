﻿namespace Airlines.Business;

public class ConsoleLoggerBusiness : IBusinessLogger
{
    public static void Log(string? output = null) => Console.Write(output);
    public static void LogLine(string? output = null) => Console.WriteLine(output);
}
