﻿namespace Airlines.Business;

public class ReadFileReturnStringList
{
    internal static List<string> FileReader(string path)
    {
        var values = new List<string>();
        try
        {
            using var reader = new StreamReader(path);
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine()!;
                values.Add(line);

            }
        }
        catch (Exception)
        {
            throw new ReadingException("Error: SreamReader throw exception!");
        }
        return values;
    }
}
