﻿using AirportEntity = Airlines.Persistence.Basic.Entities.Airport;


namespace Airlines.Business;

public static class AirportMapper
{
    public static AirportEntity ToEntity(this AirportDTO dto)
    {
        var entity = new AirportEntity
        {
            Id = dto.Id,
            AirportName = dto.AirportName,
            Country = dto.Country,
            City = dto.City,
            Code = dto.Code,
            RunwaysCount = dto.RunwaysCount,
            Founded = dto.Founded
        };

        return entity;
    }

    public static AirportDTO ToDTO(this AirportEntity entity)
    {
        var dto = new AirportDTO
        {
            Id = entity.Id,
            AirportName = entity.AirportName,
            Country = entity.Country,
            City = entity.City,
            Code = entity.Code,
            RunwaysCount = entity.RunwaysCount,
            Founded = entity.Founded
        };

        return dto;
    }
}
