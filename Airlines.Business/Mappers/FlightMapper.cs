﻿using FlightEntity = Airlines.Persistence.Basic.Entities.Flight;


namespace Airlines.Business;

public static class FlightMapper
{
    public static FlightEntity ToEntity(this FlightDTO dto)
    {
        var entity = new FlightEntity
        {
            Id = dto.Id,
            FlightNumber = dto.FlightNumber,
            FromAirportId = dto.FromAirportId,
            ToAirportId = dto.ToAirportId,
            AircraftModelId = dto.AircraftModelId,
            Price = dto.Price,
            Duration = dto.Duration,
            DepartureDatetime = dto.DepartureDatetime,
            ArrivalDatetime = dto.ArrivalDatetime,
            AirlineId = dto.AirlineId
        };

        return entity;
    }

    public static FlightDTO ToDTO(this FlightEntity entity)
    {
        var dto = new FlightDTO
        {
            Id = entity.Id,
            FlightNumber = entity.FlightNumber,
            FromAirportId = entity.FromAirportId,
            ToAirportId = entity.ToAirportId,
            AircraftModelId = entity.AircraftModelId,
            Price = entity.Price,
            Duration = entity.Duration,
            DepartureDatetime = entity.DepartureDatetime,
            ArrivalDatetime = entity.ArrivalDatetime,
            AirlineId = entity.AirlineId
        };

        return dto;
    }
}
