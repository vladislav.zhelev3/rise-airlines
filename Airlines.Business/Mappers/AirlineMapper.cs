﻿using AirlineEntity = Airlines.Persistence.Basic.Entities.Airline;


namespace Airlines.Business;

public static class AirlineMapper
{
    public static AirlineEntity ToEntity(this AirlineDTO dto)
    {
        var entity = new AirlineEntity
        {
            Id = dto.Id,
            Name = dto.Name,
            Founded = dto.Founded,
            FleetSize = dto.FleetSize,
            Description = dto.Description
        };

        return entity;
    }

    public static AirlineDTO ToDTO(this AirlineEntity entity)
    {
        var dto = new AirlineDTO
        {
            Id = entity.Id,
            Name = entity.Name,
            Founded = entity.Founded,
            FleetSize = entity.FleetSize,
            Description = entity.Description
        };

        return dto;
    }
}
