﻿using Airlines.Persistence.Basic;
using AirlineEntity = Airlines.Persistence.Basic.Entities.Airline;

namespace Airlines.Business.Service;

public class AirlineService
{
    public static List<AirlineDTO> GetAirlinesFromDB()
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        var airlines = airlineRepository.GetAirlines().Result;

        var airlinesDTOs = new List<AirlineDTO>();

        foreach (var airline in airlines)
        {
            airlinesDTOs.Add(airline.ToDTO());
        }

        return airlinesDTOs;
    }

    public static void GetAirlineByIDFromDB(int id)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        var airline = airlineRepository.GetAirlineById(id).Result;

        if (airline == null)
        {
            Console.WriteLine($"Airline with ID: {id} not found.");
        }
        else
        {
            Console.WriteLine($"Airline: {airline.Name}, Founded: {airline.Founded}, Fleet Size: {airline.FleetSize}, Description: {airline.Description}");
        }
    }

    public static void GetAirlineByNameFromDB(string name)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        var airline = airlineRepository.GetAirlineByName(name).Result;

        if (airline == null)
        {
            Console.WriteLine($"Airline with name: {name} not found.");
        }
        else
        {
            Console.WriteLine($"Airline: {airline.Name}, Founded: {airline.Founded}, Fleet Size: {airline.FleetSize}, Description: {airline.Description}");
        }
    }

    public static void GetAirlinesFoundedAfterFromDB(DateTime dateAfter)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        var airlines = airlineRepository.GetAirlinesFoundedAfter(dateAfter).Result;

        if (airlines.Count == 0)
        {
            Console.WriteLine($"No airlines found after {dateAfter}.");
        }
        else
        {
            foreach (var airline in airlines)
            {
                Console.WriteLine($"Airline: {airline.Name}, Founded: {airline.Founded}, Fleet Size: {airline.FleetSize}, Description: {airline.Description}");
            }
        }
    }

    public static void GetAirlinesFoundedBeforeFromDB(DateTime dateBefore)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        var airlines = airlineRepository.GetAirlinesFoundedBefore(dateBefore).Result;

        if (airlines.Count == 0)
        {
            Console.WriteLine($"No airlines found before {dateBefore}.");
        }
        else
        {
            foreach (var airline in airlines)
            {
                Console.WriteLine($"Airline: {airline.Name}, Founded: {airline.Founded}, Fleet Size: {airline.FleetSize}, Description: {airline.Description}");
            }
        }
    }

    public static void GetAirlinesByFleetSizeFromDB(int fleetSize)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        var airlines = airlineRepository.GetAirlinesByFleetSize(fleetSize).Result;

        if (airlines.Count == 0)
        {
            Console.WriteLine($"No airlines found with fleet size {fleetSize}.");
        }
        else
        {
            foreach (var airline in airlines)
            {
                Console.WriteLine($"Airline: {airline.Name}, Founded: {airline.Founded}, Fleet Size: {airline.FleetSize}, Description: {airline.Description}");
            }
        }
    }

    public static bool AddAirlineToDB(AirlineDTO airlineDTO)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();

        var newAirline = airlineDTO.ToEntity();

        bool success = airlineRepository.AddAirline(newAirline).Result;

        return success;
    }

    public static bool UpdateAirlineInDB(int id, AirlineDTO airlineDTO)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();

        bool success = airlineRepository.UpdateAirline(id, airlineDTO.ToEntity()).Result;

        return success;
    }

    public static void UpdateAirlineNameInDB(int id, string name)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        bool success = airlineRepository.UpdateAirlineName(id, name).Result;

        if (success)
        {
            Console.WriteLine("Airline name updated successfully.");
        }
        else
        {
            Console.WriteLine("Failed to update airline name.");
        }
    }

    public static void UpdateAirlineFoundedInDB(int id, DateTime founded)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        bool success = airlineRepository.UpdateAirlineFounded(id, founded).Result;

        if (success)
        {
            Console.WriteLine("Airline founded date updated successfully.");
        }
        else
        {
            Console.WriteLine("Failed to update airline founded date.");
        }
    }

    public static void UpdateAirlineFleetSizeInDB(int id, int fleetSize)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        bool success = airlineRepository.UpdateAirlineFleetSize(id, fleetSize).Result;

        if (success)
        {
            Console.WriteLine("Airline fleet size updated successfully.");
        }
        else
        {
            Console.WriteLine("Failed to update airline fleet size.");
        }
    }

    public static void UpdateAirlineDescriptionInDB(int id, string description)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        bool success = airlineRepository.UpdateAirlineDescription(id, description).Result;

        if (success)
        {
            Console.WriteLine("Airline description updated successfully.");
        }
        else
        {
            Console.WriteLine("Failed to update airline description.");
        }
    }


    public static bool RemoveAirlineByIDFromDB(int id)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();

        bool success = airlineRepository.DeleteAirline(id).Result;

        return success;
    }

    public static List<AirlineDTO> SearchAirlinesInDB(string searchText, string filterCriteria)
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        var airlines = airlineRepository.SearchAirlines(searchText, filterCriteria).Result;

        var airlinesDTOs = new List<AirlineDTO>();

        foreach (var airline in airlines)
        {
            airlinesDTOs.Add(airline.ToDTO());
        }

        return airlinesDTOs;
    }

    public static int? GetAirlinesCountFromDB()
    {
        var airlineRepository = ConfigurationManager.GetAirlineRepository();
        var airlinesCount = airlineRepository.GetCountAirline().Result;

        return airlinesCount;
    }
}
