﻿using Airlines.Persistence.Basic;
using FlightEntity = Airlines.Persistence.Basic.Entities.Flight;

namespace Airlines.Business.Service;

public static class FlightService
{
    public static List<FlightDTO> GetFlightsFromDB()
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flights = flightRepository.GetFlights().Result;

        var flightsDTOs = new List<FlightDTO>();

        foreach (var flight in flights)
        {
            flightsDTOs.Add(flight.ToDTO());
        }

        return flightsDTOs;
    }
    public static void GetFlightsByIDFromDB(int id)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flight = flightRepository.GetFlightById(id).Result;

        if (flight is null)
        {
            Console.WriteLine($"Flight with id: {id} is not found!");
        }
        else
        {
            Console.WriteLine($"Flight Number: {flight.FlightNumber}, Departure: {flight.FromAirportId}, Destination: {flight.ToAirportId}, Departure Time: {flight.DepartureDatetime}, Arrival Time: {flight.ArrivalDatetime}");
        }
    }

    public static void GetFlightsByFlightNumberFromDB(string flightNumber)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flight = flightRepository.GetFlightByFlightNumber(flightNumber).Result;

        if (flight is null)
        {
            Console.WriteLine($"Flight with flightNumber: {flightNumber} is not found!");
        }
        else
        {
            Console.WriteLine($"Flight Number: {flight.FlightNumber}, Departure: {flight.FromAirportId}, Destination: {flight.ToAirportId}, Departure Time: {flight.DepartureDatetime}, Arrival Time: {flight.ArrivalDatetime}");
        }
    }

    public static void GetFlightsByFromAirportIDFromDB(int fromAirportID)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flight = flightRepository.GetFlightByFromAirportID(fromAirportID).Result;

        if (flight is null)
        {
            Console.WriteLine($"Flight with fromAirportID: {fromAirportID} is not found!");
        }
        else
        {
            Console.WriteLine($"Flight Number: {flight.FlightNumber}, Departure: {flight.FromAirportId}, Destination: {flight.ToAirportId}, Departure Time: {flight.DepartureDatetime}, Arrival Time: {flight.ArrivalDatetime}");
        }
    }

    public static void GetFlightsByToAirportIDFromDB(int toAirportID)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flight = flightRepository.GetFlightByToAirportID(toAirportID).Result;

        if (flight is null)
        {
            Console.WriteLine($"Flight with toAirportID: {toAirportID} is not found!");
        }
        else
        {
            Console.WriteLine($"Flight Number: {flight.FlightNumber}, Departure: {flight.FromAirportId}, Destination: {flight.ToAirportId}, Departure Time: {flight.DepartureDatetime}, Arrival Time: {flight.ArrivalDatetime}");
        }
    }

    public static void GetFlightsByAircraftModelIDFromDB(int aircraftModelID)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flight = flightRepository.GetFlightByAircraftModelID(aircraftModelID).Result;

        if (flight is null)
        {
            Console.WriteLine($"Flight with aircraftModelID: {aircraftModelID} is not found!");
        }
        else
        {
            Console.WriteLine($"Flight Number: {flight.FlightNumber}, Departure: {flight.FromAirportId}, Destination: {flight.ToAirportId}, Departure Time: {flight.DepartureDatetime}, Arrival Time: {flight.ArrivalDatetime}");
        }
    }

    public static void GetFlightsByPriceFromDB(decimal price)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flight = flightRepository.GetFlightByPrice(price).Result;

        if (flight is null)
        {
            Console.WriteLine($"Flight with price: {price} is not found!");
        }
        else
        {
            Console.WriteLine($"Flight Number: {flight.FlightNumber}, Departure: {flight.FromAirportId}, Destination: {flight.ToAirportId}, Departure Time: {flight.DepartureDatetime}, Arrival Time: {flight.ArrivalDatetime}");
        }
    }

    public static void GetFlightsByDurationFromDB(float duration)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flight = flightRepository.GetFlightByDuration(duration).Result;

        if (flight is null)
        {
            Console.WriteLine($"Flight with duration: {duration} is not found!");
        }
        else
        {
            Console.WriteLine($"Flight Number: {flight.FlightNumber}, Departure: {flight.FromAirportId}, Destination: {flight.ToAirportId}, Departure Time: {flight.DepartureDatetime}, Arrival Time: {flight.ArrivalDatetime}");
        }
    }

    public static void GetFlightsByDepartureDatetimeFromDB(DateTime departureDatetime)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flight = flightRepository.GetFlightByDepartureDatetime(departureDatetime).Result;

        if (flight is null)
        {
            Console.WriteLine($"Flight with departureDatetime: {departureDatetime} is not found!");
        }
        else
        {
            Console.WriteLine($"Flight Number: {flight.FlightNumber}, Departure: {flight.FromAirportId}, Destination: {flight.ToAirportId}, Departure Time: {flight.DepartureDatetime}, Arrival Time: {flight.ArrivalDatetime}");
        }
    }

    public static void GetFlightsByairlineIDFromDB(int airlineID)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flight = flightRepository.GetFlightByAirlineID(airlineID).Result;

        if (flight is null)
        {
            Console.WriteLine($"Flight with airlineID: {airlineID} is not found!");
        }
        else
        {
            Console.WriteLine($"Flight Number: {flight.FlightNumber}, Departure: {flight.FromAirportId}, Destination: {flight.ToAirportId}, Departure Time: {flight.DepartureDatetime}, Arrival Time: {flight.ArrivalDatetime}");
        }
    }


    public static bool AddFlightToDB(FlightDTO flightDTO)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        var newFlight = flightDTO.ToEntity();

        bool success = flightRepository.AddFlight(newFlight).Result;

        return success;
    }


    public static bool UpdateFlightToDB(int id, FlightDTO flightDTO)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        bool success = flightRepository.UpdateFlight(id, flightDTO.ToEntity()).Result;

        return success;
    }

    public static void UpdateFlightFlightNumberToDB(int id, string flightNumber)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        bool success = flightRepository.UpdateFlightFlightNumber(id, flightNumber).Result;

        if (success)
        {
            Console.WriteLine("FlightNumber updated successfully.");
        }
        else
        {
            Console.WriteLine("FlightNumber is not updated!");
        }
    }

    public static void UpdateFlightFromAirportIDToDB(int id, int fromAirportID)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        bool success = flightRepository.UpdateFlightFromAirportID(id, fromAirportID).Result;

        if (success)
        {
            Console.WriteLine("FromAirportID updated successfully.");
        }
        else
        {
            Console.WriteLine("FromAirportID is not updated!");
        }
    }

    public static void UpdateFlightToAirportIDToDB(int id, int toAirportID)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        bool success = flightRepository.UpdateFlightToAirportID(id, toAirportID).Result;

        if (success)
        {
            Console.WriteLine("ToAirportID updated successfully.");
        }
        else
        {
            Console.WriteLine("ToAirportID is not updated!");
        }
    }

    public static void UpdateFlightAircraftModelIDToDB(int id, int aircraftModelID)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        bool success = flightRepository.UpdateFlightAircraftModelID(id, aircraftModelID).Result;

        if (success)
        {
            Console.WriteLine("AircraftModelID updated successfully.");
        }
        else
        {
            Console.WriteLine("AircraftModelID is not updated!");
        }
    }

    public static void UpdateFlightDurationToDB(int id, float duration)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        bool success = flightRepository.UpdateFlightDuration(id, duration).Result;

        if (success)
        {
            Console.WriteLine("Duration updated successfully.");
        }
        else
        {
            Console.WriteLine("Duration is not updated!");
        }
    }

    public static void UpdateFlightDepartureDatetimeToDB(int id, DateTime departureDatetime)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        bool success = flightRepository.UpdateFlightDepartureDatetime(id, departureDatetime).Result;

        if (success)
        {
            Console.WriteLine("DepartureDatetime updated successfully.");
        }
        else
        {
            Console.WriteLine("DepartureDatetime is not updated!");
        }
    }

    public static void UpdateFlightArrivalDatetimeToDB(int id, DateTime arrivalDatetime)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        bool success = flightRepository.UpdateFlightArrivalDatetime(id, arrivalDatetime).Result;

        if (success)
        {
            Console.WriteLine("ArrivalDatetime updated successfully.");
        }
        else
        {
            Console.WriteLine("ArrivalDatetime is not updated!");
        }
    }

    public static void UpdateFlightAirlineIDToDB(int id, int airlineID)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        bool success = flightRepository.UpdateFlightAirlineID(id, airlineID).Result;

        if (success)
        {
            Console.WriteLine("AirlineID updated successfully.");
        }
        else
        {
            Console.WriteLine("AirlineID is not updated!");
        }
    }

    public static bool RemoveFlightsByIDFromDB(int id)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        bool success = flightRepository.DeleteFlight(id).Result;

        return success;
    }

    public static List<FlightDTO> SearchFlightsInDB(string searchText, string filterCriteria)
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flights = flightRepository.SearchFlights(searchText, filterCriteria).Result;

        var flightsDTOs = new List<FlightDTO>();

        foreach (var flight in flights)
        {
            flightsDTOs.Add(flight.ToDTO());
        }

        return flightsDTOs;
    }

    public static int? GetFlightsCountFromDB()
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flightsCount = flightRepository.GetCountFlight().Result;

        return flightsCount;
    }
}
