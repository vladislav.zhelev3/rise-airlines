﻿using Airlines.Persistence.Basic;
using AirportEntity = Airlines.Persistence.Basic.Entities.Airport;

namespace Airlines.Business.Service;

public class AirportService
{
    public static List<AirportDTO> GetAirportsFromDB()
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();
        var airports = airportRepository.GetAirports().Result;

        var airportsDTOs = new List<AirportDTO>();

        foreach (var airport in airports)
        {
            airportsDTOs.Add(airport.ToDTO());
        }

        return airportsDTOs;
    }

    public static void GetAirportByIDFromDB(int id)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();
        var airport = airportRepository.GetAirportById(id).Result;

        if (airport == null)
        {
            Console.WriteLine($"Airport with ID: {id} not found.");
        }
        else
        {
            Console.WriteLine($"Airport: {airport.AirportName}, Code: {airport.Code}, City: {airport.City}, Country: {airport.Country}, Founded: {airport.Founded}");
        }
    }

    public static void GetAirportByCodeFromDB(string code)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();
        var airport = airportRepository.GetAirportByCode(code).Result;

        if (airport == null)
        {
            Console.WriteLine($"Airport with code: {code} not found.");
        }
        else
        {
            Console.WriteLine($"Airport: {airport.AirportName}, Code: {airport.Code}, City: {airport.City}, Country: {airport.Country}, Founded: {airport.Founded}");
        }
    }

    public static void GetAirportsByCityFromDB(string city)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();
        var airports = airportRepository.GetAirportsByCity(city).Result;

        if (airports.Count == 0)
        {
            Console.WriteLine($"No airports found in {city}.");
        }
        else
        {
            foreach (var airport in airports)
            {
                Console.WriteLine($"Airport: {airport.AirportName}, Code: {airport.Code}, City: {airport.City}, Country: {airport.Country}, Founded: {airport.Founded}");
            }
        }
    }

    public static void GetAirportsByCountryFromDB(string country)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();
        var airports = airportRepository.GetAirportsByCountry(country).Result;

        if (airports.Count == 0)
        {
            Console.WriteLine($"No airports found in {country}.");
        }
        else
        {
            foreach (var airport in airports)
            {
                Console.WriteLine($"Airport: {airport.AirportName}, Code: {airport.Code}, City: {airport.City}, Country: {airport.Country}, Founded: {airport.Founded}");
            }
        }
    }

    public static void GetAirportsFoundedAfterFromDB(DateTime foundedAfter)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();
        var airports = airportRepository.GetAirportsFoundedAfter(foundedAfter).Result;

        if (airports.Count == 0)
        {
            Console.WriteLine($"No airports found after {foundedAfter}.");
        }
        else
        {
            foreach (var airport in airports)
            {
                Console.WriteLine($"Airport: {airport.AirportName}, Code: {airport.Code}, City: {airport.City}, Country: {airport.Country}, Founded: {airport.Founded}");
            }
        }
    }

    public static void GetAirportsFoundedBeforeFromDB(DateTime foundedBefore)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();
        var airports = airportRepository.GetAirportsFoundedBefore(foundedBefore).Result;

        if (airports.Count == 0)
        {
            Console.WriteLine($"No airports found before {foundedBefore}.");
        }
        else
        {
            foreach (var airport in airports)
            {
                Console.WriteLine($"Airport: {airport.AirportName}, Code: {airport.Code}, City: {airport.City}, Country: {airport.Country}, Founded: {airport.Founded}");
            }
        }
    }

    public static bool AddAirportToDB(AirportDTO airportDTO)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();

        var newAirport = airportDTO.ToEntity();

        bool success = airportRepository.AddAirport(newAirport).Result;

        return success;
    }

    public static bool UpdateAirportToDB(int id, AirportDTO airportDTO)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();

        bool success = airportRepository.UpdateAirport(id, airportDTO.ToEntity()).Result;

        return success;
    }

    public static void UpdateAirportAirportNameToDB(int id, string airportName)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();

        bool success = airportRepository.UpdateAirportAirportName(id, airportName).Result;

        if (success)
        {
            Console.WriteLine("AirportName updated successfully.");
        }
        else
        {
            Console.WriteLine("AirportName is not updated!");
        }
    }

    public static void UpdateAirportCountryToDB(int id, string country)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();

        bool success = airportRepository.UpdateAirportCountry(id, country).Result;

        if (success)
        {
            Console.WriteLine("Country updated successfully.");
        }
        else
        {
            Console.WriteLine("Country is not updated!");
        }
    }

    public static void UpdateAirportCityToDB(int id, string city)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();

        bool success = airportRepository.UpdateAirportCity(id, city).Result;

        if (success)
        {
            Console.WriteLine("City updated successfully.");
        }
        else
        {
            Console.WriteLine("City is not updated!");
        }
    }

    public static void UpdateAirportCodeToDB(int id, string code)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();

        bool success = airportRepository.UpdateAirportCode(id, code).Result;

        if (success)
        {
            Console.WriteLine("Code updated successfully.");
        }
        else
        {
            Console.WriteLine("Code is not updated!");
        }
    }

    public static void UpdateAirportRunwaysCountToDB(int id, int runwaysCount)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();

        bool success = airportRepository.UpdateAirportRunwaysCount(id, runwaysCount).Result;

        if (success)
        {
            Console.WriteLine("RunwaysCount updated successfully.");
        }
        else
        {
            Console.WriteLine("RunwaysCount is not updated!");
        }
    }

    public static void UpdateAirportFoundedToDB(int id, DateTime founded)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();

        bool success = airportRepository.UpdateAirportFounded(id, founded).Result;

        if (success)
        {
            Console.WriteLine("Founded updated successfully.");
        }
        else
        {
            Console.WriteLine("Founded is not updated!");
        }
    }

    public static bool RemoveAirportsByIDFromDB(int id)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();

        bool success = airportRepository.DeleteAirport(id).Result;

        return success;
    }

    public static List<AirportDTO> SearchAirportsInDB(string searchText, string filterCriteria)
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();
        var airports = airportRepository.SearchAirports(searchText, filterCriteria).Result;

        var airportsDTOs = new List<AirportDTO>();

        foreach (var airport in airports)
        {
            airportsDTOs.Add(airport.ToDTO());
        }

        return airportsDTOs;
    }

    public static int? GetAirportsCountFromDB()
    {
        var airportRepository = ConfigurationManager.GetAirportRepository();
        var airportsCount = airportRepository.GetCountAirport().Result;

        return airportsCount;
    }
}
