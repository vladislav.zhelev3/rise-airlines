﻿namespace Airlines.Business;

public class Airport(string identifier, string name, string city, string country)
{
    private string _identifier = IsValidIdentifier(identifier) ? identifier : throw new AirportValidationException("Invalid airport identifier");
    private string _name = IsValidName(name) ? name : throw new AirportValidationException("Invalid airport name");
    private string _city = IsValidCity(city) ? city : throw new AirportValidationException("Invalid airport city");
    private string _country = IsValidCountry(country) ? country : throw new AirportValidationException("Invalid airport country");

    public static bool ContainsOnlyAlphabeticalOrSpaces(string input)
    {
        if (input is not null and not "")
        {
            foreach (char c in input)
            {
                if (!char.IsLetter(c) && !char.IsWhiteSpace(c))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private static bool IsValidIdentifier(string identifier) => identifier is not null && identifier.Length == 3 && identifier.All(char.IsLetterOrDigit);

    private static bool IsValidName(string name) => ContainsOnlyAlphabeticalOrSpaces(name);

    private static bool IsValidCity(string city) => ContainsOnlyAlphabeticalOrSpaces(city);

    private static bool IsValidCountry(string country) => ContainsOnlyAlphabeticalOrSpaces(country);

    public string Identifier
    {
        get => _identifier;
        set => _identifier = IsValidIdentifier(value) ? value : throw new AirportValidationException("Invalid airport identifier");
    }

    public string Name
    {
        get => _name;
        set => _name = IsValidName(value) ? value : throw new AirportValidationException("Invalid airport name");
    }

    public string City
    {
        get => _city;
        set => _city = IsValidCity(value) ? value : throw new AirportValidationException("Invalid airport city");
    }

    public string Country
    {
        get => _country;
        set => _country = IsValidCountry(value) ? value : throw new AirportValidationException("Invalid airport country");
    }
}

