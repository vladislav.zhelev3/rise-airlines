﻿namespace Airlines.Business;

public class Airline(string name)
{
    private string _name = IsValidName(name) ? name : throw new AirlineValidationException("Invalid airport name");

    private static bool ContainsOnlyAlphabeticalOrSpaces(string input)
    {
        if (input is not null and not "")
        {
            foreach (char c in input)
            {
                if (!char.IsLetter(c) && !char.IsWhiteSpace(c))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private static bool IsValidName(string name) => ContainsOnlyAlphabeticalOrSpaces(name);

    public string Name
    {
        get => _name;
        set => _name = IsValidName(value) ? value : throw new AirlineValidationException("Invalid airport name");
    }
}
