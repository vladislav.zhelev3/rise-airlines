﻿
namespace Airlines.Business;

public class Flight(string identifier, Airport departureAirport, Airport arrivalAirport, AircraftModel airctraft, string price, string durationInMinutes)
{
    private string _identifier = IsValidIdentifier(identifier) ? identifier : throw new FlightValidationException("Invalid flight identifier");
    private Airport _departureAirport = IsValidDepartureAirport(departureAirport) ? departureAirport : throw new FlightValidationException("Invalid departure airport");
    private Airport _arrivalAirport = IsValidDepartureAirport(arrivalAirport) ? arrivalAirport : throw new FlightValidationException("Invalid departure airport");
    private AircraftModel _aircraftModel = IsValidAircraftModel(airctraft) ? airctraft : throw new FlightValidationException("Invalid aircraft");
    private double _price = IsValidPrice(price) ? double.Parse(price) : throw new FlightValidationException("Invalid price type");
    private double _durationInMinutes = IsValidPrice(durationInMinutes) ? double.Parse(durationInMinutes) : throw new FlightValidationException("Invalid duration type");


    private static bool IsValidIdentifier(string identifier) => identifier is not null && identifier.Length <= 5 && identifier.Length >= 3 && identifier.All(char.IsLetterOrDigit);

    private static bool IsValidDepartureAirport(Airport departureAirport) => departureAirport != null;

    private static bool IsValidArrivalAirport(Airport arrivalAirport) => arrivalAirport != null;

    private static bool IsValidAircraftModel(AircraftModel airctraft) => airctraft != null;

    private static bool IsValidPrice(string price) => double.TryParse(price, out _);

    private static bool IsValidDuration(string durationInMinutes) => double.TryParse(durationInMinutes, out _);


    public string Identifier
    {
        get => _identifier;
        set => _identifier = IsValidIdentifier(value) ? value : throw new FlightValidationException("Invalid flight identifier");
    }

    public Airport DepartureAirport
    {
        get => _departureAirport;
        set => _departureAirport = IsValidDepartureAirport(value) ? value : throw new FlightValidationException("Invalid departure airport");
    }

    public Airport ArrivalAirport
    {
        get => _arrivalAirport;
        set => _arrivalAirport = IsValidArrivalAirport(value) ? value : throw new FlightValidationException("Invalid arrival airport");
    }

    public AircraftModel Aircraft
    {
        get => _aircraftModel;
        set => _aircraftModel = IsValidAircraftModel(value) ? value : throw new FlightValidationException("Invalid aircraft");
    }

    public double Price
    {
        get => _price;
        set => _price = IsValidPrice(value.ToString()) ? value : throw new FlightValidationException("Invalid price type");
    }

    public double DurationInMinutes
    {
        get => _durationInMinutes;
        set => _durationInMinutes = IsValidDuration(value.ToString()) ? value : throw new FlightValidationException("Invalid duration type");
    }
}

