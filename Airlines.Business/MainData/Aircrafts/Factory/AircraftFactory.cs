﻿namespace Airlines.Business;

public static class AircraftFactory
{
    internal static AircraftModel? ParseAircraft(string aircraftModel, List<string> csvAircraft)
    {
        AircraftModel? aircraft;

        string[] aircraftProps = new string[4];
        bool existInFile = false;
        foreach (var item in csvAircraft)
        {
            aircraftProps = item.Split(',');
            if (aircraftProps.Length != 4)
                throw new AircraftValidationException("Aircraft not right format!");
            if (aircraftModel.Equals(aircraftProps[0]))
            {
                existInFile = true;
                break;
            }
        }

        if (existInFile)
        {
            string model = aircraftProps[0];
            bool cargoWeightBool = double.TryParse(aircraftProps[1], out var cargoWeight);
            bool cargoVolumeBool = double.TryParse(aircraftProps[2], out var cargoVolume);
            bool seatsBool = int.TryParse(aircraftProps[3], out var seats);
            bool modelBool = !string.IsNullOrEmpty(model);

            aircraft = modelBool && cargoWeightBool && cargoVolumeBool && seatsBool
                    ? new PassengerAircraft(model, cargoWeight, cargoVolume, seats)
                    : modelBool && cargoWeightBool && cargoVolumeBool && !seatsBool
                        ? new CargoAircraft(model, cargoWeight, cargoVolume)
                        : modelBool && !cargoWeightBool && !cargoVolumeBool && seatsBool
                            ? new PrivateAircraft(model, seats)
                            : throw new AircraftValidationException("Aircraft not existing!");

            return aircraft;
        }
        throw new AircraftValidationException("Aircraft not existing!");
    }
}
