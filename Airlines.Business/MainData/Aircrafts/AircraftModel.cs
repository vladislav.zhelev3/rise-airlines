﻿namespace Airlines.Business;

public abstract class AircraftModel
{
    public abstract string Model { get; set; }
}
