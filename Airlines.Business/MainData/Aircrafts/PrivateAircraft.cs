﻿namespace Airlines.Business;

public class PrivateAircraft(string model, int seats) : AircraftModel
{
    public override string Model { get; set; } = model;

    public int Seats { get; set; } = seats;
}
