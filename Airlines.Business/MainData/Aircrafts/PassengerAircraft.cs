﻿namespace Airlines.Business;

public class PassengerAircraft(string model, double cargoWeight, double cargoVolume, int seats) : AircraftModel
{
    public override string Model { get; set; } = model;

    public double CargoWeight { get; set; } = cargoWeight;

    public double CargoVolume { get; set; } = cargoVolume;

    public int Seats { get; set; } = seats;

}
