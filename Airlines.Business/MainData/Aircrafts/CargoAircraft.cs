﻿namespace Airlines.Business;

public class CargoAircraft(string model, double cargoWeight, double cargoVolume) : AircraftModel
{
    public override string Model { get; set; } = model;

    public double CargoWeight { get; set; } = cargoWeight;

    public double CargoVolume { get; set; } = cargoVolume;

}
