namespace Airlines.Business;

public class GetAirportObjectByString
{
    internal static Airport? GetAirport(string airportIdentifier, List<Airport> airports)
    {
        Airport? refAirport = airports.Find(airport => airport.Identifier == airportIdentifier);

        return refAirport;
    }
}
