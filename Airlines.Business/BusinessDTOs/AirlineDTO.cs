﻿namespace Airlines.Business;

public class AirlineDTO
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public DateTime Founded { get; set; }

    public int FleetSize { get; set; }

    public string Description { get; set; } = null!;
}
