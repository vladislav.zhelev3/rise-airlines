﻿namespace Airlines.Business;

public class AirportDTO
{
    public int Id { get; set; }

    public string AirportName { get; set; } = null!;

    public string Country { get; set; } = null!;

    public string City { get; set; } = null!;

    public string Code { get; set; } = null!;

    public int RunwaysCount { get; set; }

    public DateTime Founded { get; set; }
}
