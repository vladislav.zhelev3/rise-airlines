﻿namespace Airlines.Business;

public class FlightDTO
{
    public int Id { get; set; }

    public string FlightNumber { get; set; } = null!;

    public int FromAirportId { get; set; }

    public int ToAirportId { get; set; }

    public int AircraftModelId { get; set; }

    public decimal Price { get; set; }

    public double Duration { get; set; }

    public DateTime DepartureDatetime { get; set; }

    public DateTime ArrivalDatetime { get; set; }

    public int AirlineId { get; set; }
}
