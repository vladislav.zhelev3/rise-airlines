﻿namespace Airlines.Business;

public class InputCommandException(string message) : Exception(message) { }
