﻿namespace Airlines.Business;

public class ReadingException(string message) : Exception(message) { }
