﻿namespace Airlines.Business;

public class FlightValidationException(string message) : ArgumentException(message) { }
