﻿namespace Airlines.Business;

public class AirportValidationException(string message) : ArgumentException(message) { }
