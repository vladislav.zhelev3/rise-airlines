﻿namespace Airlines.Business;

public class AirlineValidationException(string message) : ArgumentException(message) { }
