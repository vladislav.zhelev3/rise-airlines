namespace Airlines.Business;

public class AircraftValidationException(string message) : ArgumentException(message) { }