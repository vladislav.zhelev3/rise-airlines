namespace Airlines.Business;

public class NullOnNotNullException(string message) : Exception(message) { }
