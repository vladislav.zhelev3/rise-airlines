using Airlines.Business;

namespace Airlines.Tests;

public class SearchRouteCommandTests
{
    [Fact]
    public void Execute_Cheap_OneFlignt_ThrowsInputCommandException()
    {
        var airportGraph = new DirectedGraph<Airport, Flight>();
        var airports = new List<Airport>();
        airports.Add(new Airport("DFW", "a", "a", "a"));
        airports.Add(new Airport("JFK", "a", "a", "a"));
        airports.Add(new Airport("BKK", "a", "a", "a"));

        Vertex<Airport> vertex1 = new Vertex<Airport>(airports[0]);
        airportGraph.AddVertex(vertex1);
        Vertex<Airport> vertex2 = new Vertex<Airport>(airports[1]);
        airportGraph.AddVertex(vertex2);
        Vertex<Airport> vertex3 = new Vertex<Airport>(airports[2]);
        airportGraph.AddVertex(vertex3);

        var csvAircraft = new List<string>{ "Boing,10000,5000,-" };;

        airportGraph.AddEdge(vertex1, vertex2, new Flight("FL123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
        airportGraph.AddEdge(vertex2, vertex3, new Flight("FL124", airports[1], airports[2], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));

        var airlineNames = new HashSet<string>();
        var csvFlights = new Dictionary<string, string>();
        var flights = new LinkedList<Flight>();
        var batchCommandQueue = new Queue<ICommand>();
        var tree = new Tree<string>("DFW");


        var commandDTO = new CommandDTO(
            airports,
            airlineNames,
            csvFlights,
            flights,
            batchCommandQueue,
            csvAircraft,
            tree,
            airportGraph
        );

        string command = "groute search DFW DFW cheap";

        SearchRouteCommand searchCommand = new SearchRouteCommand(commandDTO, command);

        Assert.Throws<InputCommandException>(() => searchCommand.Execute());
    }

    [Fact]
    public void Execute_Short_OneFlignt_ThrowsInputCommandException()
    {
        var airportGraph = new DirectedGraph<Airport, Flight>();
        var airports = new List<Airport>();
        airports.Add(new Airport("DFW", "a", "a", "a"));
        airports.Add(new Airport("JFK", "a", "a", "a"));
        airports.Add(new Airport("BKK", "a", "a", "a"));

        Vertex<Airport> vertex1 = new Vertex<Airport>(airports[0]);
        airportGraph.AddVertex(vertex1);
        Vertex<Airport> vertex2 = new Vertex<Airport>(airports[1]);
        airportGraph.AddVertex(vertex2);
        Vertex<Airport> vertex3 = new Vertex<Airport>(airports[2]);
        airportGraph.AddVertex(vertex3);

        var csvAircraft = new List<string>{ "Boing,10000,5000,-" };;

        airportGraph.AddEdge(vertex1, vertex2, new Flight("FL123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
        airportGraph.AddEdge(vertex2, vertex3, new Flight("FL124", airports[1], airports[2], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));

        var airlineNames = new HashSet<string>();
        var csvFlights = new Dictionary<string, string>();
        var flights = new LinkedList<Flight>();
        var batchCommandQueue = new Queue<ICommand>();
        var tree = new Tree<string>("DFW");


        var commandDTO = new CommandDTO(
            airports,
            airlineNames,
            csvFlights,
            flights,
            batchCommandQueue,
            csvAircraft,
            tree,
            airportGraph
        );

        string command = "groute search DFW DFW short";

        SearchRouteCommand searchCommand = new SearchRouteCommand(commandDTO, command);

        Assert.Throws<InputCommandException>(() => searchCommand.Execute());
    }

    [Fact]
    public void Execute_Stops_OneFlignt_ThrowsInputCommandException()
    {
        var airportGraph = new DirectedGraph<Airport, Flight>();
        var airports = new List<Airport>();
        airports.Add(new Airport("DFW", "a", "a", "a"));
        airports.Add(new Airport("JFK", "a", "a", "a"));
        airports.Add(new Airport("BKK", "a", "a", "a"));

        Vertex<Airport> vertex1 = new Vertex<Airport>(airports[0]);
        airportGraph.AddVertex(vertex1);
        Vertex<Airport> vertex2 = new Vertex<Airport>(airports[1]);
        airportGraph.AddVertex(vertex2);
        Vertex<Airport> vertex3 = new Vertex<Airport>(airports[2]);
        airportGraph.AddVertex(vertex3);

        var csvAircraft = new List<string>{ "Boing,10000,5000,-" };;

        airportGraph.AddEdge(vertex1, vertex2, new Flight("FL123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
        airportGraph.AddEdge(vertex2, vertex3, new Flight("FL124", airports[1], airports[2], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));

        var airlineNames = new HashSet<string>();
        var csvFlights = new Dictionary<string, string>();
        var flights = new LinkedList<Flight>();
        var batchCommandQueue = new Queue<ICommand>();
        var tree = new Tree<string>("DFW");


        var commandDTO = new CommandDTO(
            airports,
            airlineNames,
            csvFlights,
            flights,
            batchCommandQueue,
            csvAircraft,
            tree,
            airportGraph
        );

        string command = "groute search DFW DFW stops";

        SearchRouteCommand searchCommand = new SearchRouteCommand(commandDTO, command);

        Assert.Throws<InputCommandException>(() => searchCommand.Execute());
    }

    [Fact]
    public void Execute_Invalid_ThrowsInputCommandException()
    {
        var airportGraph = new DirectedGraph<Airport, Flight>();
        var airports = new List<Airport>();
        airports.Add(new Airport("DFW", "a", "a", "a"));
        airports.Add(new Airport("JFK", "a", "a", "a"));
        airports.Add(new Airport("BKK", "a", "a", "a"));

        Vertex<Airport> vertex1 = new Vertex<Airport>(airports[0]);
        airportGraph.AddVertex(vertex1);
        Vertex<Airport> vertex2 = new Vertex<Airport>(airports[1]);
        airportGraph.AddVertex(vertex2);
        Vertex<Airport> vertex3 = new Vertex<Airport>(airports[2]);
        airportGraph.AddVertex(vertex3);

        var csvAircraft = new List<string>{ "Boing,10000,5000,-" };;

        airportGraph.AddEdge(vertex1, vertex2, new Flight("FL123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
        airportGraph.AddEdge(vertex2, vertex3, new Flight("FL124", airports[1], airports[2], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));

        var airlineNames = new HashSet<string>();
        var csvFlights = new Dictionary<string, string>();
        var flights = new LinkedList<Flight>();
        var batchCommandQueue = new Queue<ICommand>();
        var tree = new Tree<string>("DFW");


        var commandDTO = new CommandDTO(
            airports,
            airlineNames,
            csvFlights,
            flights,
            batchCommandQueue,
            csvAircraft,
            tree,
            airportGraph
        );

        string command = "groute search DFW DFW invalid";

        SearchRouteCommand searchCommand = new SearchRouteCommand(commandDTO, command);

        Assert.Throws<InputCommandException>(() => searchCommand.Execute());
    }

    [Fact]
    public void Execute_InvalidAirport_ThrowsInputCommandException()
    {
        var airportGraph = new DirectedGraph<Airport, Flight>();
        var airports = new List<Airport>();
        airports.Add(new Airport("DFW", "a", "a", "a"));
        airports.Add(new Airport("JFK", "a", "a", "a"));
        airports.Add(new Airport("BKK", "a", "a", "a"));

        Vertex<Airport> vertex1 = new Vertex<Airport>(airports[0]);
        airportGraph.AddVertex(vertex1);
        Vertex<Airport> vertex2 = new Vertex<Airport>(airports[1]);
        airportGraph.AddVertex(vertex2);
        Vertex<Airport> vertex3 = new Vertex<Airport>(airports[2]);
        airportGraph.AddVertex(vertex3);

        var csvAircraft = new List<string>{ "Boing,10000,5000,-" };;

        airportGraph.AddEdge(vertex1, vertex2, new Flight("FL123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
        airportGraph.AddEdge(vertex2, vertex3, new Flight("FL124", airports[1], airports[2], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));

        var airlineNames = new HashSet<string>();
        var csvFlights = new Dictionary<string, string>();
        var flights = new LinkedList<Flight>();
        var batchCommandQueue = new Queue<ICommand>();
        var tree = new Tree<string>("DFW");


        var commandDTO = new CommandDTO(
            airports,
            airlineNames,
            csvFlights,
            flights,
            batchCommandQueue,
            csvAircraft,
            tree,
            airportGraph
        );

        string command = "groute search DFW INV cheap";

        SearchRouteCommand searchCommand = new SearchRouteCommand(commandDTO, command);

        Assert.Throws<InputCommandException>(() => searchCommand.Execute());
    }

}