﻿using Airlines.Business;

namespace Airlines.Tests;

public class RoutePrintCommandTests
{
    [Fact]
    public void Execute_NoFlights_ThrowsInputCommandException()
    {
        // Arrange
        var flights = new LinkedList<Flight>();
        var commandInstance = new RoutePrintCommand(flights);

        // Act & Assert
        Assert.Throws<InputCommandException>(() => commandInstance.Execute());
    }
}
