﻿using Airlines.Business;

namespace Airlines.Tests;

public class RouteAddCommandTests
{
    [Fact]
    public void Execute_FlightNotFound_ThrowsInputCommandException()
    {
        // Arrange
        var airports = new List<Airport>();
        var airlineNames = new HashSet<string>();
        var csvFlights = new Dictionary<string, string>();
        var flights = new LinkedList<Flight>();
        var batchCommandQueue = new Queue<ICommand>();
        var csvAircraft = new List<string>();
        var tree = new Tree<string>("DFW");
        var airportGraph = new DirectedGraph<Airport, Flight>();

        var commandDTO = new CommandDTO(
            airports,
            airlineNames,
            csvFlights,
            flights,
            batchCommandQueue,
            csvAircraft,
            tree,
            airportGraph
        );


        var command = "route add ABC123";
        var commandInstance = new RouteAddCommand(commandDTO, command);

        // Act & Assert
        Assert.Throws<InputCommandException>(() => commandInstance.Execute());
    }

    [Fact]
    public void Execute_FlightAlreadyExists_ThrowsInputCommandException()
    {
        // Arrange
        var airports = new List<Airport>();
        airports.Add(new Airport("DFW", "a", "a", "a"));
        airports.Add(new Airport("JFK", "a", "a", "a"));

        var csvFlights = new Dictionary<string, string>();
        csvFlights.Add("AB123", "DFW,JFK,Boing");
        var flights = new LinkedList<Flight>();
        var csvAircraft = new List<string> { "Boing,10000,5000,4" };
        flights.AddLast(new Flight("AB123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
        
        var airlineNames = new HashSet<string>();
        var batchCommandQueue = new Queue<ICommand>();
        var tree = new Tree<string>("DFW");
        var airportGraph = new DirectedGraph<Airport, Flight>();

        var commandDTO = new CommandDTO(
            airports,
            airlineNames,
            csvFlights,
            flights,
            batchCommandQueue,
            csvAircraft,
            tree,
            airportGraph
        );

        var command = "route add FL126";
        var commandInstance = new RouteAddCommand(commandDTO, command);

        // Act & Assert
        Assert.Throws<InputCommandException>(() => commandInstance.Execute());
    }
}
