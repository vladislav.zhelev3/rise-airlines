﻿using Airlines.Business;

namespace Airlines.Tests;

public class RouteRemoveCommandTests
{
    [Fact]
    public void Execute_NoFlights_ThrowsInputCommandException()
    {
        // Arrange
        var flights = new LinkedList<Flight>();
        var commandInstance = new RouteRemoveCommand(flights);

        // Act & Assert
        Assert.Throws<InputCommandException>(() => commandInstance.Execute());
    }
}
