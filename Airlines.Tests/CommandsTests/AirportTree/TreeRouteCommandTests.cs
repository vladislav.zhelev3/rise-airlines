﻿using Airlines.Business;

namespace Airlines.Tests;

public class TreeRouteCommandTests
{

    [Fact]
    public void Execute_InvalidRoute_ThrowsException()
    {
        // Arrange
        var tree = new Tree<string>("C");
        tree.AddChild("C", "A");
        tree.AddChild("C", "B");
        string command = "tree route A B";
        var commandInstance = new TreeRouteCommand(tree, command);

        // Act & Assert
        Assert.Throws<InputCommandException>(() => commandInstance.Execute());
    }
}
