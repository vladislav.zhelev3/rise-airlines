﻿using Airlines.Business;

namespace Airlines.Tests;

public class ReserveTicketCommandTests
{
    [Fact]
    public void Execute_NoFlights_ThrowsInputCommandException()
    {
        // Arrange
        var flights = new LinkedList<Flight>();
        var command = "reserve ticket ABC123 2 10 20";
        var commandInstance = new ReserveTicketCommand(flights, command);

        // Act & Assert
        Assert.Throws<InputCommandException>(() => commandInstance.Execute());
    }

    [Fact]
    public void Execute_FlightsExistButNoMatchingFlight_ThrowsInputCommandException()
    {
        // Arrange
        var airports = new List<Airport>();
        airports.Add( new Airport("DFW", "a", "a", "a"));
        airports.Add( new Airport("JFK", "a", "a", "a"));

        var flights = new LinkedList<Flight>();
        var csvAircraft = new List<string> { "Boing,10000,5000,-" };
        flights.AddLast(new Flight("XY123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
        var command = "reserve ticket AB123 2 10 20";
        var commandInstance = new ReserveTicketCommand(flights, command);

        // Act & Assert
        Assert.Throws<InputCommandException>(() => commandInstance.Execute());
    }

    [Fact]
    public void Execute_FlightExists_InvalidBaggage_ThrowsInputCommandException()
    {
        // Arrange
        var airports = new List<Airport>();
        airports.Add( new Airport("DFW", "a", "a", "a"));
        airports.Add( new Airport("JFK", "a", "a", "a"));

        var flights = new LinkedList<Flight>();
        var csvAircraft = new List<string> { "Boing,10000,5000,-" };
        flights.AddLast(new Flight("AB123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
        var command = "reserve ticket AB123 2 40 20";
        var commandInstance = new ReserveTicketCommand(flights, command);

        // Act & Assert
        Assert.Throws<InputCommandException>(() => commandInstance.Execute());
    }
}
