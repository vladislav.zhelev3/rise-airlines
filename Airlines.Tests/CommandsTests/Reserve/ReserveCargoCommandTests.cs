﻿using Airlines.Business;

namespace Airlines.Tests;

public class ReserveCargoCommandTests
{
    [Fact]
    public void Execute_NoFlights_ThrowsInputCommandException()
    {
        // Arrange
        var flights = new LinkedList<Flight>();
        var command = "reserve cargo AB123 100 200";
        var commandInstance = new ReserveCargoCommand(flights, command);

        // Act & Assert
        // Ensure that executing the command throws an InputCommandException
        Assert.Throws<InputCommandException>(() => commandInstance.Execute());
    }

    [Fact]
    public void Execute_FlightsExistButNoMatchingFlight_ThrowsInputCommandException()
    {
        // Arrange
        var airports = new List<Airport>();
        airports.Add( new Airport("DFW", "a", "a", "a"));
        airports.Add( new Airport("JFK", "a", "a", "a"));

        var flights = new LinkedList<Flight>();
        var csvAircraft = new List<string> { "Boing,10000,5000,-" };
        flights.AddLast(new Flight("XY123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
        var command = "reserve cargo AB123 100 200";
        var commandInstance = new ReserveCargoCommand(flights, command);

        // Act & Assert
        Assert.Throws<InputCommandException>(() => commandInstance.Execute());
    }
}
