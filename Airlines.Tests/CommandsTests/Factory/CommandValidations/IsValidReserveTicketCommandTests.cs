﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidReserveTicketCommandTests
{
    [Theory]
    [InlineData("reserve ticket ABC123 2 20 30", true)]
    [InlineData("Reserve Ticket XYZ456 1 15.5 25.75", true)]
    [InlineData("reserve ticket 123XYZ 2 20 30", true)]
    [InlineData("reserve ticket ABC123 XYZ 20 30", false)]
    [InlineData("reserve ticket ABC123 2 XYZ 30", false)]
    [InlineData("reserve ticket ABC123 2 20 XYZ", false)]
    [InlineData("reserve ticket ABC123", false)]
    [InlineData("", false)]
    [InlineData("reserve", false)]
    [InlineData("notreserve ticket ABC123 2 20 30", false)]
    [InlineData("reserve ticket ABC123 2 20 30 40", false)]
    public void IsValidReserveTicketCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidReserveTicketCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
