﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidTreeRouteCommandTests
{
    [Theory]
    [InlineData("tree route AAA BBB", true)]
    [InlineData("Tree Route AAA CCC", true)]
    [InlineData("tree route AB CCC", false)]
    [InlineData("tree route AAA AB", false)]
    [InlineData("tree", false)]
    [InlineData("", false)]
    [InlineData("nottree route AAA BBB", false)]
    [InlineData("tree route AAA BBB CCC", false)]
    public void IsValidTreeRouteCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidTreeRouteCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
