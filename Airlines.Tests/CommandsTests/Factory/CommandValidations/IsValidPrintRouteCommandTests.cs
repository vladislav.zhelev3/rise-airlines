﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidPrintRouteCommandTests
{
    [Theory]
    [InlineData("route print", true)]
    [InlineData("Route Print", true)]
    [InlineData("route", false)]
    [InlineData("", false)]
    [InlineData("notroute print", false)]
    [InlineData("route print SomeOtherParam", false)]
    public void IsValidPrintRouteCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidPrintRouteCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
