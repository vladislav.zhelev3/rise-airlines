﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidAddRouteCommandTests
{
    [Theory]
    [InlineData("route add", false)]
    [InlineData("route add parameter", true)]
    [InlineData("route Add", false)]
    [InlineData("route add param1 param2", false)]
    [InlineData("route", false)]
    [InlineData("", false)]
    [InlineData("notroute add", false)]
    [InlineData("route add some other param", false)]
    public void IsValidAddRouteCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidAddRouteCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
