using Airlines.Business;

namespace Airlines.Tests;

public class IsValidGraphRouteSearchCommandTests
{
    [Theory]
    [InlineData("groute search ABC XYZ cheap", true)]
    [InlineData("GRouTe SeArCh abc xyz cheap", true)]
    [InlineData("groute search ABC XYZ short", true)]
    [InlineData("groute search ABC XYZ stops", true)]
    [InlineData("groute search AB XYZ cheap", false)]
    [InlineData("groute search ABCD XYZ cheap", false)]
    [InlineData("groute search ABC XYZ", false)]
    [InlineData("groute search ABC XYZ fast", false)]
    [InlineData("groute search ABC XYZ shortest", false)]
    [InlineData("groute search ABC", false)]
    [InlineData("groute search", false)]
    [InlineData("search ABC XYZ cheap", false)]
    [InlineData("", false)]
    [InlineData(null, false)]
    public void IsValidGraphRouteSearchCommand_WithDifferentCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        bool result = CommandValidator.IsValidGraphRouteSearchCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}

