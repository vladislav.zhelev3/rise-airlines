﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidHelpCommandTests
{
    [Theory]
    [InlineData("help", true)]
    [InlineData("Help", true)]
    [InlineData("not_help", false)]
    [InlineData("hel p", false)]
    [InlineData("", false)]
    [InlineData(" ", false)]
    public void IsValidHelpCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidHelpCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
