﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidExitCommandTests
{
    [Theory]
    [InlineData("exit", true)]
    [InlineData("Exit", true)]
    [InlineData("not_exit", false)]
    [InlineData("exi t", false)]
    [InlineData("", false)]
    [InlineData(" ", false)]
    public void IsValidExitCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidExitCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
