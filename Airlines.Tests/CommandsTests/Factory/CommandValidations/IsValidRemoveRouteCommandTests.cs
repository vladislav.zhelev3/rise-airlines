﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidRemoveRouteCommandTests
{
    [Theory]
    [InlineData("route remove", true)]
    [InlineData("Route Remove", true)]
    [InlineData("route", false)]
    [InlineData("", false)]
    [InlineData("notroute remove", false)]
    [InlineData("route remove SomeOtherParam", false)]
    public void IsValidRemoveRouteCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidRemoveRouteCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
