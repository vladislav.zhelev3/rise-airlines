﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidNewRouteCommandTests
{
    [Theory]
    [InlineData("route new", true)]
    [InlineData("Route New", true)]
    [InlineData("route", false)]
    [InlineData("", false)]
    [InlineData("notroute new", false)]
    [InlineData("route new SomeOtherParam", false)]
    public void IsValidNewRouteCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidNewRouteCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
