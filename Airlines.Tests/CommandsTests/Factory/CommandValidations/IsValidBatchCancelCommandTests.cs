﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidBatchCancelCommandTests
{
    [Theory]
    [InlineData("batch cancel", true)]
    [InlineData("Batch Cancel", true)]
    [InlineData("batch", false)]
    [InlineData("", false)]
    [InlineData("notbatch cancel", false)]
    [InlineData("batch cancel SomeOtherParam", false)]
    public void IsValidBatchCancelCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidBatchCancelCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
