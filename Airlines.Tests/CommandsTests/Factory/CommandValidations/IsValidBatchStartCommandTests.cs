﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidBatchStartCommandTests
{
    [Theory]
    [InlineData("batch start", true)]
    [InlineData("Batch Start", true)]
    [InlineData("batch", false)]
    [InlineData("", false)]
    [InlineData("notbatch start", false)]
    [InlineData("batch start SomeOtherParam", false)]
    public void IsValidBatchStartCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidBatchStartCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
