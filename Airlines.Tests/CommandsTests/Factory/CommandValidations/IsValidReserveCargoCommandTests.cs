﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidReserveCargoCommandTests
{
    [Theory]
    [InlineData("reserve cargo ABC123 100 200", true)]
    [InlineData("Reserve Cargo XYZ456 250.5 300.75", true)]
    [InlineData("reserve cargo 123XYZ 100 200", true)]
    [InlineData("reserve cargo ABC123 100 XYZ", false)]
    [InlineData("reserve cargo ABC123 XYZ 200", true)]
    [InlineData("reserve cargo ABC123", false)]
    [InlineData("", false)]
    [InlineData("reserve", false)]
    [InlineData("notreserve cargo ABC123 100 200", false)]
    [InlineData("reserve cargo ABC123 100 200 300", false)]
    public void IsValidReserveCargoCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidReserveCargoCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
