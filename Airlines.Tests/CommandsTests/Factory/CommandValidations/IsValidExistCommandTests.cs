﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidExistCommandTests
{
    [Theory]
    [InlineData("exist SomeAirline", true)] 
    [InlineData("Exist SomeAirline", true)]
    [InlineData("exist", false)] 
    [InlineData("", false)]
    [InlineData("notexist SomeAirline", false)] 
    [InlineData("Exist SomeAirline OtherParam", false)]
    public void IsValidExistCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidExistCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
