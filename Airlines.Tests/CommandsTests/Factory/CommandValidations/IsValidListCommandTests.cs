﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidListCommandTests
{
    [Theory]
    [InlineData("list airports city", true)]
    [InlineData("List Airports City", true)]
    [InlineData("list airports country", true)]
    [InlineData("list", false)] 
    [InlineData("", false)]
    [InlineData("notlist airports city", false)]
    [InlineData("list airports", false)]
    [InlineData("list airports someotherparam", false)]
    public void IsValidListCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidListCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
