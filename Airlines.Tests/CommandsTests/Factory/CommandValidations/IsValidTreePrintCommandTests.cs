﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidTreePrintCommandTests
{
    [Theory]
    [InlineData("tree print", true)]
    [InlineData("Tree Print", true)]
    [InlineData("tree", false)]
    [InlineData("", false)]
    [InlineData("nottree print", false)]
    [InlineData("tree print SomeOtherParam", false)]
    public void IsValidTreePrintCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidTreePrintCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
