using Airlines.Business;

namespace Airlines.Tests;

public class YourClassTests
{
    [Theory]
    [InlineData("groute check ABC XYZ", true)]
    [InlineData("GRouTe ChEcK abc xyz", true)]
    [InlineData("groute check AB XYZ", false)]
    [InlineData("groute check ABCD XYZ", false)]
    [InlineData("groute check ABC", false)]
    [InlineData("groute check", false)]
    [InlineData("check ABC XYZ", false)]
    [InlineData("", false)]
    [InlineData(null, false)]
    public void IsValidGraphRouteCheckCommand_WithDifferentCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        bool result = CommandValidator.IsValidGraphRouteCheckCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}

