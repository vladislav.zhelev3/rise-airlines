using Airlines.Business;

namespace Airlines.Tests;

public class IsValidGraphRoutePrintCommandTests
{
    [Theory]
    [InlineData("groute print", true)]
    [InlineData("GRouTe PrInT", true)]
    [InlineData("groute  print", false)]
    [InlineData("groute", false)]
    [InlineData("print", false)]
    [InlineData("", false)]
    [InlineData(null, false)]
    public void IsValidGraphRoutePrintCommand_WithDifferentCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        bool result = CommandValidator.IsValidGraphRoutePrintCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}

