﻿using Airlines.Business;

namespace Airlines.Tests;

public class IsValidBatchRunCommandTests
{
    [Theory]
    [InlineData("batch run", true)]
    [InlineData("Batch Run", true)]
    [InlineData("batch", false)]
    [InlineData("", false)]
    [InlineData("notbatch run", false)]
    [InlineData("batch run SomeOtherParam", false)]
    public void IsValidBatchRunCommand_ValidAndInvalidCommands_ReturnsExpectedResult(string command, bool expectedResult)
    {
        // Act
        var result = CommandValidator.IsValidBatchRunCommand(command);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
