using Xunit;
using System.Collections.Generic;
using System.Linq;
using Airlines.Business;

namespace Airlines.Tests
{
    public class CommandFactoryTests
    {
        [Fact]
        public void Parse_ExistCommand_ValidCommand_ReturnsExistCommandObject()
        {
            // Arrange
            var airports = new List<Airport>();
            var airlineName = new HashSet<string>();
            var csvFlights = new Dictionary<string, string>();
            var flights = new LinkedList<Flight>();
            var batchCommandQueue = new Queue<ICommand>();
            var csvAircraft = new List<string>();
            var tree = new Tree<string>("DFW");
            var airportGraph = new DirectedGraph<Airport, Flight>();

            var commandDto = new CommandDTO(airports, airlineName, csvFlights, flights, batchCommandQueue, csvAircraft, tree, airportGraph);
            var command = "exist SomeAirline";

            // Act
            var result = CommandFactory.Parse(commandDto, command);

            // Assert
            Assert.IsType<ExistCommand>(result);
        }

        // Add more test cases for other commands...

        [Fact]
        public void Parse_InvalidCommand_ReturnsNotFoundCommandObject()
        {
            // Arrange
            var airports = new List<Airport>();
            var airlineName = new HashSet<string>();
            var csvFlights = new Dictionary<string, string>();
            var flights = new LinkedList<Flight>();
            var batchCommandQueue = new Queue<ICommand>();
            var csvAircraft = new List<string>();
            var tree = new Tree<string>("DFW");
            var airportGraph = new DirectedGraph<Airport, Flight>();

            var commandDto = new CommandDTO(airports, airlineName, csvFlights, flights, batchCommandQueue, csvAircraft, tree, airportGraph);

            var command = "invalid command";

            // Act
            var result = CommandFactory.Parse(commandDto, command);

            // Assert
            Assert.IsType<NotFoundCommand>(result);
        }

        [Fact]
        public void Parse_ExistingCommand_ValidCommand_ReturnsExistCommandObject()
        {
            // Arrange
            var airports = new List<Airport>();
            var airlineName = new HashSet<string>();
            var csvFlights = new Dictionary<string, string>();
            var flights = new LinkedList<Flight>();
            var batchCommandQueue = new Queue<ICommand>();
            var csvAircraft = new List<string>();
            var tree = new Tree<string>("DFW");
            var airportGraph = new DirectedGraph<Airport, Flight>();

            var commandDto = new CommandDTO(airports, airlineName, csvFlights, flights, batchCommandQueue, csvAircraft, tree, airportGraph);
            var command = "exist SomeAirline";
            
            // Act
            var result = CommandFactory.Parse(commandDto, command);

            // Assert
            Assert.IsType<ExistCommand>(result);
        }

        [Fact]
        public void Parse_ListCommand_ValidCommand_ReturnsListCommandObject()
        {
            // Arrange
            var airports = new List<Airport>();
            var airlineName = new HashSet<string>();
            var csvFlights = new Dictionary<string, string>();
            var flights = new LinkedList<Flight>();
            var batchCommandQueue = new Queue<ICommand>();
            var csvAircraft = new List<string>();
            var tree = new Tree<string>("DFW");
            var airportGraph = new DirectedGraph<Airport, Flight>();

            var commandDto = new CommandDTO(airports, airlineName, csvFlights, flights, batchCommandQueue, csvAircraft, tree, airportGraph);
            var command = "list airports";

            // Act
            var result = CommandFactory.Parse(commandDto, command);

            // Assert
            Assert.IsType<NotFoundCommand>(result);
        }

        [Fact]
        public void Parse_InvalidCommand_ReturnsNotFoundCommandObjects()
        {
            // Arrange
            var airports = new List<Airport>();
            var airlineName = new HashSet<string>();
            var csvFlights = new Dictionary<string, string>();
            var flights = new LinkedList<Flight>();
            var batchCommandQueue = new Queue<ICommand>();
            var csvAircraft = new List<string>();
            var tree = new Tree<string>("DFW");
            var airportGraph = new DirectedGraph<Airport, Flight>();

            var commandDto = new CommandDTO(airports, airlineName, csvFlights, flights, batchCommandQueue, csvAircraft, tree, airportGraph);
            var command = "invalid command";

            // Act
            var result = CommandFactory.Parse(commandDto, command);

            // Assert
            Assert.IsType<NotFoundCommand>(result);
        }


    }
}
