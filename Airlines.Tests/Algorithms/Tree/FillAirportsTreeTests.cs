﻿using Airlines.Business;

namespace Airlines.Tests;

public class FillAirportsTreeTests
{
    [Fact]
    public void FillTreeWithAirports_AddsAllAirportsToTree()
    {
        // Arrange
        var tree = new Tree<string>("DFW");
        var csvFlights = new Dictionary<string, string>
            {
                { "FL535", "DFW,JFK,Bombardier Challenger 300" },
                { "FL505", "DFW,ORD,Boeing 787-9" },
                { "FL126", "DFW,LAX,Boeing 747-8F" },
                { "FL457", "DFW,ATL,Airbus A320" },
                { "FL785", "JFK,FRA,Gulfstream G650" },
                { "FL101", "JFK,AMS,Boeing 737-700C" },
            };

        // Act
        tree.FillTreeWithAirports(csvFlights);

        // Assert
        Assert.Equal(4, tree.Root.Children.Count);
    }

    [Fact]
    public void FillTreeWithAirports_AddsCorrectChildNodes()
    {
        // Arrange
        var tree = new Tree<string>("DFW");
        var csvFlights = new Dictionary<string, string>
            {
                { "FL535", "DFW,JFK,Bombardier Challenger 300" },
                { "FL505", "DFW,ORD,Boeing 787-9" },
                { "FL126", "DFW,LAX,Boeing 747-8F" },
            };

        // Act
        tree.FillTreeWithAirports(csvFlights);

        // Assert
        Assert.Equal(3, tree.Root.Children.Count);
    }
}
