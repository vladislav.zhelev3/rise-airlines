using System.Collections.Generic;
using Xunit;

namespace Airlines.Business.Tests
{
    public class CheckForPossibleRouteTests
    {
        [Fact]
        public void CheckRoute_WithValidAirports_ReturnsTrue()
        {
            // Arrange
            var csvAircraft = new List<string> { "Boing,10000,5000,-" };

            var airportGraph = new DirectedGraph<Airport, Flight>();
            var airports = new List<Airport>();
            airports.Add(new Airport("DFW", "a", "a", "a"));
            airports.Add(new Airport("JFK", "a", "a", "a"));
            airports.Add(new Airport("BKK", "a", "a", "a"));

            Vertex<Airport> vertex1 = new Vertex<Airport>(airports[0]);
            airportGraph.AddVertex(vertex1);
            Vertex<Airport> vertex2 = new Vertex<Airport>(airports[1]);
            airportGraph.AddVertex(vertex2);
            Vertex<Airport> vertex3 = new Vertex<Airport>(airports[2]);
            airportGraph.AddVertex(vertex3);

            airportGraph.AddEdge(vertex1, vertex2, new Flight("FL123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
            airportGraph.AddEdge(vertex2, vertex3, new Flight("FL124", airports[1], airports[2], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));

            // Act
            bool result = airportGraph.CheckRoute("DFW", "BKK");

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void CheckRoute_WithValidAirports_ReturnsFalse()
        {
            // Arrange
            var csvAircraft = new List<string> { "Boing,10000,5000,-" };

            var airportGraph = new DirectedGraph<Airport, Flight>();
            var airports = new List<Airport>();
            airports.Add(new Airport("DFW", "a", "a", "a"));
            airports.Add(new Airport("JFK", "a", "a", "a"));
            airports.Add(new Airport("BKK", "a", "a", "a"));

            Vertex<Airport> vertex1 = new Vertex<Airport>(airports[0]);
            airportGraph.AddVertex(vertex1);
            Vertex<Airport> vertex2 = new Vertex<Airport>(airports[1]);
            airportGraph.AddVertex(vertex2);
            Vertex<Airport> vertex3 = new Vertex<Airport>(airports[2]);
            airportGraph.AddVertex(vertex3);

            airportGraph.AddEdge(vertex1, vertex2, new Flight("FL123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
            airportGraph.AddEdge(vertex2, vertex3, new Flight("FL124", airports[1], airports[2], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));

            // Act
            bool result = airportGraph.CheckRoute("BKK", "DFW");

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void CheckRoute_WithInvalidAirports_ThrowsInputCommandException()
        {
            // Arrange
            var csvAircraft = new List<string> { "Boing,10000,5000,-" };

            var airportGraph = new DirectedGraph<Airport, Flight>();
            var airports = new List<Airport>();
            airports.Add(new Airport("DFW", "a", "a", "a"));
            airports.Add(new Airport("JFK", "a", "a", "a"));
            airports.Add(new Airport("BKK", "a", "a", "a"));

            Vertex<Airport> vertex1 = new Vertex<Airport>(airports[0]);
            airportGraph.AddVertex(vertex1);
            Vertex<Airport> vertex2 = new Vertex<Airport>(airports[1]);
            airportGraph.AddVertex(vertex2);
            Vertex<Airport> vertex3 = new Vertex<Airport>(airports[2]);
            airportGraph.AddVertex(vertex3);

            airportGraph.AddEdge(vertex1, vertex2, new Flight("FL123", airports[0], airports[1], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));
            airportGraph.AddEdge(vertex2, vertex3, new Flight("FL124", airports[1], airports[2], AircraftFactory.ParseAircraft("Boing", csvAircraft)!, "1", "1"));

            // Act & Assert
            Assert.Throws<InputCommandException>(() => airportGraph.CheckRoute("X", "Y"));
        }

    }
}
