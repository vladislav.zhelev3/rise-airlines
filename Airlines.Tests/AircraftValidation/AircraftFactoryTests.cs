﻿using Airlines.Business;

namespace Airlines.Tests;

public class AircraftFactoryTests
{
    [Fact]
    public void ParseAircraft_PrivateAircraft_ShouldReturnCorrectModel()
    {
        // Arrange
        string aircraftModel = "Private";
        var csvAircraft = new List<string> { "Private,-,-,5" };

        // Act
        var aircraft = AircraftFactory.ParseAircraft(aircraftModel, csvAircraft);

        // Assert
        Assert.IsType<PrivateAircraft>(aircraft);
        Assert.Equal("Private", aircraft.Model);
        Assert.Equal(5, ((PrivateAircraft)aircraft).Seats);
    }

    [Fact]
    public void ParseAircraft_CargoAircraft_ShouldReturnCorrectModel()
    {
        // Arrange
        string aircraftModel = "Cargo";
        var csvAircraft = new List<string> { "Cargo,10000,1000,-" };

        // Act
        var aircraft = AircraftFactory.ParseAircraft(aircraftModel, csvAircraft);

        // Assert
        Assert.IsType<CargoAircraft>(aircraft);
        Assert.Equal("Cargo", aircraft.Model);
        Assert.Equal(10000, ((CargoAircraft)aircraft).CargoWeight);
        Assert.Equal(1000, ((CargoAircraft)aircraft).CargoVolume);
    }

    [Fact]
    public void ParseAircraft_PassengerAircraft_ShouldReturnCorrectModel()
    {
        // Arrange
        string aircraftModel = "Passenger";
        var csvAircraft = new List<string> { "Passenger,5000,500,200" };

        // Act
        var aircraft = AircraftFactory.ParseAircraft(aircraftModel, csvAircraft);

        // Assert
        Assert.IsType<PassengerAircraft>(aircraft);
        Assert.Equal("Passenger", aircraft.Model);
        Assert.Equal(5000, ((PassengerAircraft)aircraft).CargoWeight);
        Assert.Equal(500, ((PassengerAircraft)aircraft).CargoVolume);
        Assert.Equal(200, ((PassengerAircraft)aircraft).Seats);
    }

    [Fact]
    public void ParseAircraft_UnknownAircraftModel_ShouldThrowException()
    {
        // Arrange
        string aircraftModel = "Unknown";
        var csvAircraft = new List<string> { "Private,-,-,5" };       

        // Assert
        Assert.Throws<AircraftValidationException>(() => AircraftFactory.ParseAircraft(aircraftModel, csvAircraft));
    }

    [Fact]
    public void ParseAircraft_NotRightProps_ShouldThrowException()
    {
        // Arrange
        string aircraftModel = "Private";
        var csvAircraft = new List<string> { "Private,-,-,67,5" };

        // Assert
        Assert.Throws<AircraftValidationException>(() => AircraftFactory.ParseAircraft(aircraftModel, csvAircraft));
    }
}
