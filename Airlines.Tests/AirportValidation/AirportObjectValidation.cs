﻿using Airlines.Business;

namespace Airlines.Tests;

public class AirportObjectValidation
{
    [Fact]
    public void Constructor_ValidArguments_ShouldSetCorrectly()
    {
        // Arrange
        string identifier = "ABC";
        string name = "Airport Name";
        string city = "City";
        string country = "Country";

        // Act
        var airport = new Airport(identifier, name, city, country);

        // Assert
        Assert.Equal(identifier, airport.Identifier);
        Assert.Equal(name, airport.Name);
        Assert.Equal(city, airport.City);
        Assert.Equal(country, airport.Country);
    }

    [Theory]
    [InlineData(null, "Airport Name", "City", "Country")]
    [InlineData("ABC", null, "City", "Country")]
    [InlineData("ABC", "Airport Name", null, "Country")]
    [InlineData("ABC", "Airport Name", "City", null)]
    [InlineData("", "Airport Name", "City", "Country")]
    [InlineData("ABC", "", "City", "Country")]
    [InlineData("ABC", "Airport Name", "", "Country")]
    [InlineData("ABC", "Airport Name", "City", "")]
    [InlineData("1234", "Airport Name", "City", "Country")]
    [InlineData("ABCD", "Airport Name", "City", "Country")]
    [InlineData("ABC", "Airport*Name", "City", "Country")]
    [InlineData("ABC", "Airport Name", "City*123", "Country")]
    [InlineData("ABC", "Airport Name", "City", "Country$")]
    public void Constructor_InvalidArguments_ShouldThrowException(string identifier, string name, string city, string country)
    {
        // Act & Assert
        Assert.Throws<AirportValidationException>(() => new Airport(identifier, name, city, country));
    }

    [Fact]
    public void Identifier_SetValidIdentifier_ShouldSetCorrectly()
    {
        // Arrange
        var airport = new Airport("INI", "Name", "City", "Country");
        string newIdentifier = "NiN";

        // Act
        airport.Identifier = newIdentifier;

        // Assert
        Assert.Equal(newIdentifier, airport.Identifier);
    }

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    [InlineData("ABCD")]
    [InlineData("ABC*")]
    public void Identifier_SetInvalidIdentifier_ShouldThrowException(string newIdentifier)
    {
        // Arrange
        var airport = new Airport("INI", "Name", "City", "Country");

        // Act & Assert
        Assert.Throws<AirportValidationException>(() => airport.Identifier = newIdentifier);
    }

    [Fact]
    public void Name_SetValidName_ShouldSetCorrectly()
    {
        // Arrange
        var airport = new Airport("INI", "Initial Name", "City", "Country");
        string validName = "Valid Airport Name";

        // Act
        airport.Name = validName;

        // Assert
        Assert.Equal(validName, airport.Name);
    }

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    [InlineData("123")]
    [InlineData("Airport*Name")]
    public void Name_SetInvalidName_ShouldThrowException(string invalidName)
    {
        // Arrange
        var airport = new Airport("INI", "Initial Name", "City", "Country");

        // Act & Assert
        Assert.Throws<AirportValidationException>(() => airport.Name = invalidName);
    }

    [Fact]
    public void City_SetValidCity_ShouldSetCorrectly()
    {
        // Arrange
        var airport = new Airport("INI", "Name", "Initial City", "Country");
        string validCity = "Valid City";

        // Act
        airport.City = validCity;

        // Assert
        Assert.Equal(validCity, airport.City);
    }

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    [InlineData("123")]
    [InlineData("City*123")]
    public void City_SetInvalidCity_ShouldThrowException(string invalidCity)
    {
        // Arrange
        var airport = new Airport("INI", "Name", "Initial City", "Country");

        // Act & Assert
        Assert.Throws<AirportValidationException>(() => airport.City = invalidCity);
    }

    [Fact]
    public void Country_SetValidCountry_ShouldSetCorrectly()
    {
        // Arrange
        var airport = new Airport("INI", "Name", "City", "Initial Country");
        string validCountry = "Valid Country";

        // Act
        airport.Country = validCountry;

        // Assert
        Assert.Equal(validCountry, airport.Country);
    }

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    [InlineData("123")]
    [InlineData("Country$")]
    public void Country_SetInvalidCountry_ShouldThrowException(string invalidCountry)
    {
        // Arrange
        var airport = new Airport("INI", "Name", "City", "Initial Country");

        // Act & Assert
        Assert.Throws<AirportValidationException>(() => airport.Country = invalidCountry);
    }
}


