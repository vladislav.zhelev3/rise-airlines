using Airlines.Business;

namespace Airlines.Tests;

public class TreeFunctionsTests
{


    [Fact]
    public void AddChild_AddsChildCorrectly()
    {
        // Arrange
        var tree = new Tree<string>("Root");

        // Act
        tree.AddChild("Root", "Child1");

        // Assert
        Assert.Single(tree.Root.Children);
        Assert.Equal("Child1", tree.Root.Children[0].Data);
    }

    [Fact]
    public void Remove_RemovesNodeCorrectly()
    {
        // Arrange
        var tree = new Tree<string>("Root");
        tree.AddChild("Root", "Child1");
        tree.AddChild("Root", "Child2");

        // Act
        tree.Remove("Child1");

        // Assert
        Assert.Single(tree.Root.Children);
        Assert.Equal("Child2", tree.Root.Children[0].Data);
    }

    [Fact]
    public void Traverse_TraversesTreeCorrectly()
    {
        // Arrange
        var tree = new Tree<string>("Root");
        tree.AddChild("Root", "Child1");
        tree.AddChild("Root", "Child2");
        tree.AddChild("Child1", "Grandchild1");
        tree.AddChild("Child1", "Grandchild2");
        tree.AddChild("Child2", "Grandchild3");

        var traversedNodes = new List<string>();

        // Act
        tree.Traverse(node => traversedNodes.Add(node));

        // Assert
        Assert.Collection(traversedNodes,
            node => Assert.Equal("Root", node),
            node => Assert.Equal("Child1", node),
            node => Assert.Equal("Grandchild1", node),
            node => Assert.Equal("Grandchild2", node),
            node => Assert.Equal("Child2", node),
            node => Assert.Equal("Grandchild3", node)
        );
    }

    [Fact]
    public void CanReach_FindsCorrectRoute()
    {
        // Arrange
        var tree = new Tree<string>("Root");
        tree.AddChild("Root", "Child1");
        tree.AddChild("Root", "Child2");
        tree.AddChild("Child1", "Grandchild1");
        tree.AddChild("Child1", "Grandchild2");
        tree.AddChild("Child2", "Grandchild3");

        // Act
        var route = tree.CanReach("Root", "Grandchild3");

        // Assert
        Assert.NotNull(route);
        Assert.Collection(route!,
            node => Assert.Equal("Root", node),
            node => Assert.Equal("Child2", node),
            node => Assert.Equal("Grandchild3", node)
        );
    }

    [Fact]
    public void CanReach_ReturnsNullIfNoRouteExists()
    {
        // Arrange
        var tree = new Tree<string>("Root");
        tree.AddChild("Root", "Child1");
        tree.AddChild("Root", "Child2");
        tree.AddChild("Child1", "Grandchild1");
        tree.AddChild("Child1", "Grandchild2");
        tree.AddChild("Child2", "Grandchild3");

        // Act
        var route = tree.CanReach("Child1", "Grandchild3");

        // Assert
        Assert.Null(route);
    }
}