﻿using Airlines.Business;

namespace Airlines.Tests;

public class AirlineObjectValidation
{
    [Fact]
        public void Constructor_ValidName_ShouldSetCorrectly()
        {
            // Arrange
            string validName = "Valid Airline Name";

            // Act
            var airline = new Airline(validName);

            // Assert
            Assert.Equal(validName, airline.Name);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("123")]
        [InlineData("Name*")]
        public void Constructor_InvalidName_ShouldThrowException(string invalidName)
        {
            // Act & Assert
            Assert.Throws<AirlineValidationException>(() => new Airline(invalidName));
        }

        [Fact]
        public void Name_SetValidName_ShouldSetCorrectly()
        {
            // Arrange
            var airline = new Airline("Initial Name");
            string validName = "Valid Airline Name";

            // Act
            airline.Name = validName;

            // Assert
            Assert.Equal(validName, airline.Name);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("123")]
        [InlineData("Name*")]
        public void Name_SetInvalidName_ShouldThrowException(string invalidName)
        {
            // Arrange
            var airline = new Airline("Initial Name");

            // Act & Assert
            Assert.Throws<AirlineValidationException>(() => airline.Name = invalidName);
        }
}
