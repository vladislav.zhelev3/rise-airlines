﻿using Airlines.Business;

namespace Airlines.Tests;

public class FileReader
{
    [Fact]
    public void FileReader_ReturnsCorrectValues()
    {
        // Arrange
        var expectedValues = new List<string> { "line1", "line2", "line3" };
        var path = "../../../CSVTestFiles/test.csv";
        File.WriteAllLines(path, expectedValues);

        // Act
        var result = ReadFileReturnStringList.FileReader(path);

        // Assert
        Assert.Equal(expectedValues, result);
    }

    [Fact]
    public void FileReader_EmptyArray_ReturnsCorrectValues()
    {
        // Arrange
        var expectedValues = new List<string> {};
        var path = "../../../CSVTestFiles/testEmpty.csv";
        File.WriteAllLines(path, expectedValues);

        // Act
        var result = ReadFileReturnStringList.FileReader(path);

        // Assert
        Assert.Equal(expectedValues, result);
    }

    [Fact]
    public void FileReader_NotExistingFile_ThrowException()
    {
        // Arrange
        var path = "../../../CSVTestFiles/NotExistingFile.csv";

        // Assert
        Assert.Throws<ReadingException>(() => ReadFileReturnStringList.FileReader(path));
    }


}
