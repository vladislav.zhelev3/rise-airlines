using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Airlines.Business;
using Airlines.Business.Service;

namespace Airlines.API.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class AirlineController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetAll()
        {
            List<AirlineDTO> airlines = AirlineService.GetAirlinesFromDB();
            if (airlines.Count == 0)
            {
                NotFound();
            }
            else if (airlines == null)
            {
                StatusCode(500, new { message = "Internal server error." });
            }

            return Ok(airlines);
        }

        [HttpPost]
        public IActionResult Create([FromBody] AirlineDTO newAirline)
        {
            var success = AirlineService.AddAirlineToDB(newAirline);

            if (!success)
            {
                return StatusCode(500, new { message = "Internal server error." });
            }
            return CreatedAtAction(nameof(GetAll), new { id = newAirline.Id }, newAirline);
        }

        [HttpGet("{searchText}")]
        public IActionResult Search(string searchText, string filterCriteria)
        {
            var airlines = AirlineService.SearchAirlinesInDB(searchText, filterCriteria);

            if (airlines.Count == 0)
            {
                NotFound();
            }
            else if (airlines == null)
            {
                StatusCode(500, new { message = "Internal server error." });
            }
            return Ok(airlines);
        }
        
        [HttpPut]
        public IActionResult Update(int id, [FromBody] AirlineDTO airline) 
        {
            var success = AirlineService.UpdateAirlineInDB(id, airline);
            if (!success)
            {
                return StatusCode(500, new { message = "Internal server error." });
            }
            return Ok();
        }
        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var success = AirlineService.RemoveAirlineByIDFromDB(id);
            if (!success)
            {
                return StatusCode(500, new { message = "Internal server error." });
            }
            return Ok();
        }
    }
}
