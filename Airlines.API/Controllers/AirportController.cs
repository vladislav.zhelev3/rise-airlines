using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Airlines.Business;
using Airlines.Business.Service;

namespace Airlines.API.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class AirportController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetAll()
        {
            List<AirportDTO> airports = AirportService.GetAirportsFromDB();
            if (airports.Count == 0)
            {
                NotFound();
            }
            else if (airports == null)
            {
                StatusCode(500, new { message = "Internal server error." });
            }

            return Ok(airports);
        }

        [HttpPost]
        public IActionResult Create([FromBody] AirportDTO newAirport)
        {
            var success = AirportService.AddAirportToDB(newAirport);

            if (!success)
            {
                return StatusCode(500, new { message = "Internal server error." });
            }
            return CreatedAtAction(nameof(GetAll), new { id = newAirport.Id }, newAirport);
        }

        [HttpGet("{searchText}")]
        public IActionResult Search(string searchText, string filterCriteria)
        {
            var airports = AirportService.SearchAirportsInDB(searchText, filterCriteria);

            if (airports.Count == 0)
            {
                NotFound();
            }
            else if (airports == null)
            {
                StatusCode(500, new { message = "Internal server error." });
            }
            return Ok(airports);
        }
        
        [HttpPut]
        public IActionResult Update(int id, [FromBody] AirportDTO airport) 
        {
            var success = AirportService.UpdateAirportToDB(id, airport);
            if (!success)
            {
                return StatusCode(500, new { message = "Internal server error." });
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var success = AirportService.RemoveAirportsByIDFromDB(id);
            if (!success)
            {
                return StatusCode(500, new { message = "Internal server error." });
            }
            return Ok();
        }
    }
}
