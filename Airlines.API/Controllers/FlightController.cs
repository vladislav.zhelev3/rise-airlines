using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Airlines.Business;
using Airlines.Business.Service;

namespace Airlines.API.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetAll()
        {
            List<FlightDTO> flights = FlightService.GetFlightsFromDB();
            if (flights.Count == 0)
            { 
                NotFound();
            }
            else if (flights == null)
            {
                StatusCode(500, new { message = "Internal server error." });
            }

            return Ok(flights);
        }

        [HttpPost]
        public IActionResult Create([FromBody] FlightDTO newFlight)
        {
            var success = FlightService.AddFlightToDB(newFlight);

            if (!success)
            {
                return StatusCode(500, new { message = "Internal server error." });
            }
            return CreatedAtAction(nameof(GetAll), new { id = newFlight.Id }, newFlight);
        }

        [HttpGet("{searchText}")]
        public IActionResult Search(string searchText, string filterCriteria)
        {
            var flights = FlightService.SearchFlightsInDB(searchText, filterCriteria);

            if (flights.Count == 0)
            {
                NotFound();
            }
            else if (flights == null)
            {
                StatusCode(500, new { message = "Internal server error." });
            }
            return Ok(flights);
        }
        
        [HttpPut]
        public IActionResult Update(int id, [FromBody] FlightDTO flight) 
        {
            var success = FlightService.UpdateFlightToDB(id, flight);
            if (!success)
            {
                return StatusCode(500, new { message = "Internal server error." });
            }
            return Ok();
        }
        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var success = FlightService.RemoveFlightsByIDFromDB(id);
            if (!success)
            {
                return StatusCode(500, new { message = "Internal server error." });
            }
            return Ok();
        }
    }
}
