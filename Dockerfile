FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
USER app
WORKDIR /app
EXPOSE 8080

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["AirlinesWeb/AirlinesWeb.csproj", "AirlinesWeb/"]
COPY ["Airlines.Business/Airlines.Business.csproj", "Airlines.Business/"]
COPY ["Airlines.Persistence.Basic/Airlines.Persistence.Basic.csproj", "Airlines.Persistence.Basic/"]
RUN dotnet restore "./AirlinesWeb/AirlinesWeb.csproj"
COPY . .
WORKDIR "/src/AirlinesWeb"
RUN dotnet build "./AirlinesWeb.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./AirlinesWeb.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "AirlinesWeb.dll"]
