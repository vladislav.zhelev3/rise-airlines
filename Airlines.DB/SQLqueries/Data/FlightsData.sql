INSERT INTO Flights (Flight_number, From_Airport, To_Airport, Departure_datetime, Arrival_datetime)
VALUES 
    ('FL123', 'SOF', 'CIA', '2024-04-10 08:00:00', '2024-04-10 10:00:00'),
    ('FL456', 'CIA', 'SOF', '2024-04-10 12:00:00', '2024-04-10 14:00:00');


SELECT * FROM Flights WHERE Flight_number = 'FL123';


UPDATE Flights SET To_Airport = 'FCO' WHERE Flight_number = 'FL456';


DELETE FROM Flights WHERE Flight_number = 'FL123';