SELECT 
    F.flightNumber,
    A1.Code AS fromAirport,
    A2.Code AS toAirport,
    AM.name AS aircraftModel,
    ASE.seats AS totalSeats,
    ASD.weight AS aircraftWeight,
    ASD.volume AS aircraftVolume,
    F.price,
    F.duration,
    F.departureDatetime,
    F.arrivalDatetime,
    AL.Name AS airlineName
FROM Flights F
JOIN Airports A1 ON F.fromAirportID = A1.id
JOIN Airports A2 ON F.toAirportID = A2.id
JOIN AircraftModels AM ON F.aircraftModelID = AM.id
JOIN Airlines AL ON F.airlineID = AL.id
LEFT JOIN AircraftSeats ASE ON F.aircraftModelID = ASE.aircraftModelID
LEFT JOIN AircraftSpaceDimentions ASD ON F.aircraftModelID = ASD.aircraftModelID
WHERE CONVERT(DATE, F.departureDatetime) = CONVERT(DATE, GETDATE() + 1);
-- WHERE datediff(DAY, GETDATE(), F.departureDatetime) = 1

SELECT COUNT(*) AS DepartedFlightsCount
FROM Flights
WHERE departureDatetime < GETDATE();

SELECT TOP 1
    F.flightNumber,
    F.duration,
    A1.Code AS fromAirport,
    A2.Code AS toAirport
FROM Flights F
JOIN Airports A1 ON F.fromAirportID = A1.id
JOIN Airports A2 ON F.toAirportID = A2.id
ORDER BY F.duration DESC;