ALTER TABLE dbo.Flights
ADD FOREIGN KEY (fromAirportID) REFERENCES dbo.Airports (id);

ALTER TABLE dbo.Flights
ADD FOREIGN KEY (toAirportID) REFERENCES dbo.Airports (id);


ALTER TABLE dbo.Flights
ADD CONSTRAINT CheckDepartureTimeIsValid CHECK(departureDatetime >= GETDATE());

ALTER TABLE dbo.Flights
ADD CONSTRAINT CheckArrivalTimeIsValid CHECK(arrivalDatetime >= departureDatetime);