CREATE TABLE AircraftModels (
    [id] INTEGER IDENTITY(1,1) PRIMARY KEY,
    [name] VARCHAR(256) NOT NULL
);

CREATE TABLE AircraftSeats (
    [aircraftModelID] INT PRIMARY KEY,
    [seats] INT NOT NULL
);

CREATE TABLE AircraftSpaceDimentions (
    [aircraftModelID] INT PRIMARY KEY,
    [weight] DECIMAL NOT NULL,
    [volume] DECIMAL NOT NULL
);