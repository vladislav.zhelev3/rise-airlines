ALTER TABLE dbo.AircraftSeats
ADD FOREIGN KEY (aircraftModelID) REFERENCES dbo.AircraftModels (id);

ALTER TABLE dbo.AircraftSpaceDimentions
ADD FOREIGN KEY (aircraftModelID) REFERENCES dbo.AircraftModels (id);

ALTER TABLE dbo.Flights
ADD FOREIGN KEY (aircraftModelID) REFERENCES dbo.AircraftModels (id);