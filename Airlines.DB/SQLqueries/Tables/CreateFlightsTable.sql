CREATE TABLE Flights (
    id INT IDENTITY(1,1) PRIMARY KEY,
    flightNumber VARCHAR(256) UNIQUE NOT NULL,
    fromAirportID INT NOT NULL,
    toAirportID INT NOT NULL,
    aircraftModelID INT NOT NULL,
    price DECIMAL NOT NULL,
    duration FLOAT NOT NULL,
    departureDatetime DATETIME2 NOT NULL,
    arrivalDatetime DATETIME2 NOT NULL,
    airlineID INT NOT NULL,
);