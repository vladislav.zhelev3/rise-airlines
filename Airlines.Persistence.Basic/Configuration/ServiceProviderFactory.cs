using Microsoft.EntityFrameworkCore;
using Airlines.Persistence.Basic.Entities;
using Microsoft.Extensions.DependencyInjection;


namespace Airlines.Persistence.Basic;

public class ServiceProviderFactory
{
    private static IServiceProvider? _serviceProvider;

    public static IServiceProvider GetServiceProvider()
    {
        if (_serviceProvider == null)
        {
            // Create service collection
            var services = new ServiceCollection();

            // Add services
            services.AddScoped<IFlightRepository, FlightRepository>(); // Add repository as a scoped service
            services.AddScoped<IAirportRepository, AirportRepository>();
            services.AddScoped<IAirlineRepository, AirlineRepository>();

            services.AddDbContext<AirlinesDbContext>(options =>
                options.UseSqlServer(ConfigurationManager.GetConnectionString("Local")));

            // Build service provider
            _serviceProvider = services.BuildServiceProvider();
        }

        return _serviceProvider;
    }
}

