using Microsoft.Extensions.Configuration;
using Airlines.Persistence.Basic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;


namespace Airlines.Persistence.Basic;

public class ConfigurationManager
{
    private static IConfigurationRoot _configuration;

    static ConfigurationManager()
    {

        string basePath = AppDomain.CurrentDomain.BaseDirectory;
        string relativePath = "../../../../Airlines.Persistence.Basic/appsettings.json"; // - without docker
        // string relativePath = "appsettings.json"; // docker

        // Build configuration once
        _configuration = new ConfigurationBuilder()
            .AddJsonFile(Path.Combine(basePath, relativePath))
            .Build();
    }

    public static string? GetConnectionString(string name)
    {
        // Get connection string from configuration
        return _configuration.GetConnectionString(name);
    }

    public static IFlightRepository GetFlightRepository()
    {

        // Get service provider
        var serviceProvider = ServiceProviderFactory.GetServiceProvider();

        // Resolve repository
        var repository = serviceProvider.GetRequiredService<IFlightRepository>();

        return repository;
    }

    public static IAirportRepository GetAirportRepository()
    {

        // Get service provider
        var serviceProvider = ServiceProviderFactory.GetServiceProvider();

        // Resolve repository
        var repository = serviceProvider.GetRequiredService<IAirportRepository>();

        return repository;
    }

    public static IAirlineRepository GetAirlineRepository()
    {

        // Get service provider
        var serviceProvider = ServiceProviderFactory.GetServiceProvider();

        // Resolve repository
        var repository = serviceProvider.GetRequiredService<IAirlineRepository>();

        return repository;
    }
}
