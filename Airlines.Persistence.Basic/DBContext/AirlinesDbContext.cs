﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Basic.Entities;

public partial class AirlinesDbContext : DbContext
{
    public AirlinesDbContext()
    {
    }

    public AirlinesDbContext(DbContextOptions<AirlinesDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<AircraftModel> AircraftModels { get; set; }

    public virtual DbSet<AircraftSeat> AircraftSeats { get; set; }

    public virtual DbSet<AircraftSpaceDimention> AircraftSpaceDimentions { get; set; }

    public virtual DbSet<Airline> Airlines { get; set; }

    public virtual DbSet<Airport> Airports { get; set; }

    public virtual DbSet<Flight> Flights { get; set; }

//     protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
// #warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
//         => optionsBuilder.UseSqlServer("data source=localhost;initial catalog=AirlinesDB;integrated security=False;MultipleActiveResultSets=True;Encrypt=False;User ID=sa;Password=Base8989;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AircraftModel>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Aircraft__3213E83F3224940A");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Name)
                .HasMaxLength(256)
                .IsUnicode(false)
                .HasColumnName("name");
        });

        modelBuilder.Entity<AircraftSeat>(entity =>
        {
            entity.HasKey(e => e.AircraftModelId).HasName("PK__Aircraft__4F02319CD60F2301");

            entity.Property(e => e.AircraftModelId)
                .ValueGeneratedNever()
                .HasColumnName("aircraftModelID");
            entity.Property(e => e.Seats).HasColumnName("seats");

            entity.HasOne(d => d.AircraftModel).WithOne(p => p.AircraftSeat)
                .HasForeignKey<AircraftSeat>(d => d.AircraftModelId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__AircraftS__aircr__7B5B524B");
        });

        modelBuilder.Entity<AircraftSpaceDimention>(entity =>
        {
            entity.HasKey(e => e.AircraftModelId).HasName("PK__Aircraft__4F02319C1A9F901B");

            entity.Property(e => e.AircraftModelId)
                .ValueGeneratedNever()
                .HasColumnName("aircraftModelID");
            entity.Property(e => e.Volume)
                .HasColumnType("decimal(18, 0)")
                .HasColumnName("volume");
            entity.Property(e => e.Weight)
                .HasColumnType("decimal(18, 0)")
                .HasColumnName("weight");

            entity.HasOne(d => d.AircraftModel).WithOne(p => p.AircraftSpaceDimention)
                .HasForeignKey<AircraftSpaceDimention>(d => d.AircraftModelId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__AircraftS__aircr__7C4F7684");
        });

        modelBuilder.Entity<Airline>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Airlines__3213E83FCC29F522");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Description).HasColumnType("text");
            entity.Property(e => e.Name)
                .HasMaxLength(256)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Airport>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Airports__3213E83FDD421310");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AirportName)
                .HasMaxLength(256)
                .IsUnicode(false)
                .HasColumnName("airportName");
            entity.Property(e => e.City)
                .HasMaxLength(256)
                .IsUnicode(false)
                .HasColumnName("city");
            entity.Property(e => e.Code)
                .HasMaxLength(3)
                .IsUnicode(false);
            entity.Property(e => e.Country)
                .HasMaxLength(256)
                .IsUnicode(false)
                .HasColumnName("country");
            entity.Property(e => e.Founded).HasColumnName("founded");
            entity.Property(e => e.RunwaysCount).HasColumnName("runwaysCount");
        });

        modelBuilder.Entity<Flight>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__Flights__3213E83F50D2C83B");

            entity.HasIndex(e => e.FlightNumber, "UQ__Flights__4E642B64F9F03E45").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AircraftModelId).HasColumnName("aircraftModelID");
            entity.Property(e => e.AirlineId).HasColumnName("airlineID");
            entity.Property(e => e.ArrivalDatetime).HasColumnName("arrivalDatetime");
            entity.Property(e => e.DepartureDatetime).HasColumnName("departureDatetime");
            entity.Property(e => e.Duration).HasColumnName("duration");
            entity.Property(e => e.FlightNumber)
                .HasMaxLength(256)
                .IsUnicode(false)
                .HasColumnName("flightNumber");
            entity.Property(e => e.FromAirportId).HasColumnName("fromAirportID");
            entity.Property(e => e.Price)
                .HasColumnType("decimal(18, 0)")
                .HasColumnName("price");
            entity.Property(e => e.ToAirportId).HasColumnName("toAirportID");

            entity.HasOne(d => d.FromAirport).WithMany(p => p.FlightFromAirports)
                .HasForeignKey(d => d.FromAirportId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Flights__fromAir__245D67DE");

            entity.HasOne(d => d.ToAirport).WithMany(p => p.FlightToAirports)
                .HasForeignKey(d => d.ToAirportId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Flights__toAirpo__25518C17");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
