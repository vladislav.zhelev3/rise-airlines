﻿using System;
using System.Collections.Generic;

namespace Airlines.Persistence.Basic.Entities;

public partial class Airline
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public DateTime Founded { get; set; }

    public int FleetSize { get; set; }

    public string Description { get; set; } = null!;
}
