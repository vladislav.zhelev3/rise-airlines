﻿using System;
using System.Collections.Generic;

namespace Airlines.Persistence.Basic.Entities;

public partial class AircraftSeat
{
    public int AircraftModelId { get; set; }

    public int Seats { get; set; }

    public virtual AircraftModel AircraftModel { get; set; } = null!;
}
