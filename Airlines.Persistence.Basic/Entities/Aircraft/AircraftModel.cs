﻿using System;
using System.Collections.Generic;

namespace Airlines.Persistence.Basic.Entities;

public partial class AircraftModel
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public virtual AircraftSeat? AircraftSeat { get; set; }

    public virtual AircraftSpaceDimention? AircraftSpaceDimention { get; set; }
}
