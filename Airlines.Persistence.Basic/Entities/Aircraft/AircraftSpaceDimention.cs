﻿using System;
using System.Collections.Generic;

namespace Airlines.Persistence.Basic.Entities;

public partial class AircraftSpaceDimention
{
    public int AircraftModelId { get; set; }

    public decimal Weight { get; set; }

    public decimal Volume { get; set; }

    public virtual AircraftModel AircraftModel { get; set; } = null!;
}
