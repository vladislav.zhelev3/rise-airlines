﻿using System;
using System.Collections.Generic;

namespace Airlines.Persistence.Basic.Entities;

public partial class Airport
{
    public int Id { get; set; }

    public string AirportName { get; set; } = null!;

    public string Country { get; set; } = null!;

    public string City { get; set; } = null!;

    public string Code { get; set; } = null!;

    public int RunwaysCount { get; set; }

    public DateTime Founded { get; set; }

    public virtual ICollection<Flight> FlightFromAirports { get; set; } = new List<Flight>();

    public virtual ICollection<Flight> FlightToAirports { get; set; } = new List<Flight>();
}
