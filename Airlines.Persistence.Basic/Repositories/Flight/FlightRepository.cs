﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Reflection;
using System.Linq.Expressions;

using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic;

public class FlightRepository : IFlightRepository
{
    private readonly AirlinesDbContext _context;

    public FlightRepository(AirlinesDbContext context)
    {
        _context = context;
    }

    public async Task<List<Flight>> GetFlights()
    {
        var response = await _context.Flights.ToListAsync();
        return response;
    }

    public async Task<Flight?> GetFlightById(int id)
    {
        var response = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);
        return response;
    }

    public async Task<Flight?> GetFlightByFlightNumber(string flightNumber)
    {
        var response = await _context.Flights.FirstOrDefaultAsync(f => f.FlightNumber == flightNumber);
        return response;
    }

    public async Task<Flight?> GetFlightByFromAirportID(int fromAirportID)
    {
        var response = await _context.Flights.FirstOrDefaultAsync(f => f.FromAirportId == fromAirportID);
        return response;
    }

    public async Task<Flight?> GetFlightByToAirportID(int toAirportID)
    {
        var response = await _context.Flights.FirstOrDefaultAsync(f => f.ToAirportId == toAirportID);
        return response;
    }

    public async Task<Flight?> GetFlightByAircraftModelID(int aircraftModelID)
    {
        var response = await _context.Flights.FirstOrDefaultAsync(f => f.AircraftModelId == aircraftModelID);
        return response;
    }

    public async Task<Flight?> GetFlightByPrice(decimal price)
    {
        var response = await _context.Flights.FirstOrDefaultAsync(f => f.Price == price);
        return response;
    }

    public async Task<Flight?> GetFlightByDuration(float duration)
    {
        var response = await _context.Flights.FirstOrDefaultAsync(f => f.Duration == duration);
        return response;
    }

    public async Task<Flight?> GetFlightByDepartureDatetime(DateTime departureDatetime)
    {
        var response = await _context.Flights.FirstOrDefaultAsync(f => f.DepartureDatetime == departureDatetime);
        return response;
    }

    public async Task<Flight?> GetFlightByArrivalDatetime(DateTime arrivalDatetime)
    {
        var response = await _context.Flights.FirstOrDefaultAsync(f => f.ArrivalDatetime == arrivalDatetime);
        return response;
    }

    public async Task<Flight?> GetFlightByAirlineID(int airlineID)
    {
        var response = await _context.Flights.FirstOrDefaultAsync(f => f.AirlineId == airlineID);
        return response;
    }

    public async Task<bool> AddFlight(Flight flight)
    {
        try
        {
            await _context.Flights.AddAsync(flight);

            bool success = await _context.SaveChangesAsync() > 0;

            return true;
        }
        catch (System.Exception)
        {
            return false;
        }
    }

    public async Task<bool> UpdateFlight(int id, Flight flight)
    {

        var newFlight = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);


        if (newFlight is null) return false;

        newFlight.FlightNumber = flight.FlightNumber;
        newFlight.FromAirportId = flight.FromAirportId;
        newFlight.ToAirportId = flight.ToAirportId;
        newFlight.AircraftModelId = flight.AircraftModelId;
        newFlight.Price = flight.Price;
        newFlight.Duration = flight.Duration;
        newFlight.DepartureDatetime = flight.DepartureDatetime;
        newFlight.ArrivalDatetime = flight.ArrivalDatetime;
        newFlight.AirlineId = flight.AirlineId;

        _context.Flights.Update(newFlight);

        bool success = await _context.SaveChangesAsync() > 0;

        return true;
    }

    public async Task<bool> UpdateFlightFlightNumber(int id, string flightNumber)
    {
        var flight = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);

        if (flight is null) return false;

        flight.FlightNumber = flightNumber;

        bool success = await _context.SaveChangesAsync() > 0;

        return true;
    }

    public async Task<bool> UpdateFlightFromAirportID(int id, int fromAirportID)
    {
        var flight = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);

        if (flight is null) return false;

        flight.FromAirportId = fromAirportID;

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateFlightToAirportID(int id, int toAirportID)
    {
        var flight = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);

        if (flight is null) return false;

        flight.ToAirportId = toAirportID;

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateFlightAircraftModelID(int id, int aircraftModelId)
    {
        var flight = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);

        if (flight is null) return false;

        flight.AircraftModelId = aircraftModelId;

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateFlightPrice(int id, decimal price)
    {
        var flight = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);

        if (flight is null) return false;

        flight.Price = price;

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateFlightDuration(int id, float duration)
    {
        var flight = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);

        if (flight is null) return false;

        flight.Duration = duration;

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateFlightDepartureDatetime(int id, DateTime departureDateTime)
    {
        var flight = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);

        if (flight is null) return false;

        flight.DepartureDatetime = departureDateTime;

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateFlightArrivalDatetime(int id, DateTime arrivalDateTime)
    {
        var flight = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);

        if (flight is null) return false;

        flight.ArrivalDatetime = arrivalDateTime;

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateFlightAirlineID(int id, int airlineID)
    {
        var flight = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);

        if (flight is null) return false;

        flight.AirlineId = airlineID;

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeleteFlight(int id)
    {
        try
        {
            Flight? flight = await _context.Flights.FirstOrDefaultAsync(f => f.Id == id);

            if (flight is null) return false;

            _context.Flights.Remove(flight);

            bool success = await _context.SaveChangesAsync() > 0;

            return true;
        }
        catch (System.Exception)
        {
            return false;
        }
    }

    public async Task<List<Flight>> SearchFlights(string searchText, string filterCriteria)
    {
        Expression<Func<Flight, bool>> filterExpression = filterCriteria switch
        {
            "FlightNumber" => (a => a.FlightNumber.Contains(searchText)),
            "FromAirportId" => (a => a.FromAirportId.ToString().Contains(searchText)),
            "ToAirportId" => (a => a.ToAirportId.ToString().Contains(searchText)),
            "AircraftModelId" => (a => a.AircraftModelId.ToString().Contains(searchText)),
            "Price" => (a => a.Price.ToString().Contains(searchText)),
            "Duration" => (a => a.Duration.ToString().Contains(searchText)),
            "DepartureDatetime" => (a => a.DepartureDatetime.ToString().Contains(searchText)),
            "ArrivalDatetime" => (a => a.ArrivalDatetime.ToString().Contains(searchText)),
            "AirlineId" => (a => a.AirlineId.ToString().Contains(searchText)),
            _ => throw new ArgumentException("Invalid filter criteria")
        };

        return await _context.Flights.Where(filterExpression).ToListAsync();
    }

    public async Task<int> GetCountFlight()
    {
        return await _context.Flights.CountAsync();
    }
}
