using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic;

public interface IFlightRepository
{
    Task<List<Flight>> GetFlights();
    Task<Flight?> GetFlightById(int id);
    Task<Flight?> GetFlightByFlightNumber(string flightNumber);
    Task<Flight?> GetFlightByFromAirportID(int fromAirportID);
    Task<Flight?> GetFlightByToAirportID(int toAirportID);
    Task<Flight?> GetFlightByAircraftModelID(int aircraftModelID);
    Task<Flight?> GetFlightByPrice(decimal price);
    Task<Flight?> GetFlightByDuration(float duration);
    Task<Flight?> GetFlightByDepartureDatetime(DateTime departureDatetime);
    Task<Flight?> GetFlightByArrivalDatetime(DateTime arrivalDatetime);
    Task<Flight?> GetFlightByAirlineID(int airlineID);

    Task<bool> AddFlight(Flight flight);

    Task<bool> UpdateFlight(int id, Flight flight);
    Task<bool> UpdateFlightFlightNumber(int id, string flightNumber);
    Task<bool> UpdateFlightFromAirportID(int id, int fromAirportID);
    Task<bool> UpdateFlightToAirportID(int id, int toAirportID);
    Task<bool> UpdateFlightAircraftModelID(int id, int aircraftModelId);
    Task<bool> UpdateFlightPrice(int id, decimal price);
    Task<bool> UpdateFlightDuration(int id, float duration);
    Task<bool> UpdateFlightDepartureDatetime(int id, DateTime departureDateTime);
    Task<bool> UpdateFlightArrivalDatetime(int id, DateTime arrivalDateTime);
    Task<bool> UpdateFlightAirlineID(int id, int airlineID);

    Task<bool> DeleteFlight(int id);

    Task<List<Flight>> SearchFlights(string searchText, string filterCriteria);

    Task<int> GetCountFlight();
}
