﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic;

public interface IAirportRepository
{
    Task<List<Airport>> GetAirports();
    Task<Airport?> GetAirportById(int id);
    Task<Airport?> GetAirportByCode(string code);
    Task<List<Airport>> GetAirportsByCountry(string country);
    Task<List<Airport>> GetAirportsByCity(string city);
    Task<List<Airport>> GetAirportsFoundedAfter(DateTime dateAfter);
    Task<List<Airport>> GetAirportsFoundedBefore(DateTime dateBefore);

    Task<bool> AddAirport(Airport airport);

    Task<bool> UpdateAirport(int id, Airport airport);
    Task<bool> UpdateAirportAirportName(int id, string airportName);
    Task<bool> UpdateAirportCountry(int id, string country);
    Task<bool> UpdateAirportCity(int id, string city);
    Task<bool> UpdateAirportCode(int id, string code);
    Task<bool> UpdateAirportRunwaysCount(int id, int runwaysCount);
    Task<bool> UpdateAirportFounded(int id, DateTime founded);

    Task<bool> DeleteAirport(int id);

    Task<List<Airport>> SearchAirports(string searchText, string filterCriteria);

    Task<int> GetCountAirport();
}
