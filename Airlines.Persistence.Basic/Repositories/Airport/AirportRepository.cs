﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using System.Linq.Expressions;

using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic;

public class AirportRepository : IAirportRepository
{
    private readonly AirlinesDbContext _context;

    public AirportRepository(AirlinesDbContext context)
    {
        _context = context;
    }

    public async Task<List<Airport>> GetAirports()
    {
        return await _context.Airports.ToListAsync();
    }

    public async Task<Airport?> GetAirportById(int id)
    {
        return await _context.Airports.FirstOrDefaultAsync(a => a.Id == id);
    }

    public async Task<Airport?> GetAirportByCode(string code)
    {
        return await _context.Airports.FirstOrDefaultAsync(a => a.Code == code);
    }

    public async Task<List<Airport>> GetAirportsByCountry(string country)
    {
        return await _context.Airports.Where(a => a.Country == country).ToListAsync();
    }

    public async Task<List<Airport>> GetAirportsByCity(string city)
    {
        return await _context.Airports.Where(a => a.City == city).ToListAsync();
    }

    public async Task<List<Airport>> GetAirportsFoundedAfter(DateTime dateAfter)
    {
        return await _context.Airports.Where(a => a.Founded > dateAfter).ToListAsync();
    }

    public async Task<List<Airport>> GetAirportsFoundedBefore(DateTime dateBefore)
    {
        return await _context.Airports.Where(a => a.Founded < dateBefore).ToListAsync();
    }

    public async Task<bool> AddAirport(Airport airport)
    {
        try
        {
            await _context.Airports.AddAsync(airport);
            return await _context.SaveChangesAsync() > 0;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public async Task<bool> UpdateAirport(int id, Airport airport)
    {
        var existingAirport = await _context.Airports.FirstOrDefaultAsync(a => a.Id == id);
        if (existingAirport == null) return false;

        existingAirport.AirportName = airport.AirportName;
        existingAirport.Country = airport.Country;
        existingAirport.City = airport.City;
        existingAirport.Code = airport.Code;
        existingAirport.RunwaysCount = airport.RunwaysCount;
        existingAirport.Founded = airport.Founded;

        _context.Airports.Update(existingAirport);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateAirportAirportName(int id, string airportName)
    {
        var airport = await _context.Airports.FirstOrDefaultAsync(a => a.Id == id);
        if (airport == null) return false;

        airport.AirportName = airportName;
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateAirportCountry(int id, string country)
    {
        var airport = await _context.Airports.FirstOrDefaultAsync(a => a.Id == id);
        if (airport == null) return false;

        airport.Country = country;
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateAirportCity(int id, string city)
    {
        var airport = await _context.Airports.FirstOrDefaultAsync(a => a.Id == id);
        if (airport == null) return false;

        airport.City = city;
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateAirportCode(int id, string code)
    {
        var airport = await _context.Airports.FirstOrDefaultAsync(a => a.Id == id);
        if (airport == null) return false;

        airport.Code = code;
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateAirportRunwaysCount(int id, int runwaysCount)
    {
        var airport = await _context.Airports.FirstOrDefaultAsync(a => a.Id == id);
        if (airport == null) return false;

        airport.RunwaysCount = runwaysCount;
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateAirportFounded(int id, DateTime founded)
    {
        var airport = await _context.Airports.FirstOrDefaultAsync(a => a.Id == id);
        if (airport == null) return false;

        airport.Founded = founded;
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeleteAirport(int id)
    {
        try
        {
            var airport = await _context.Airports.FirstOrDefaultAsync(a => a.Id == id);
            if (airport == null) return false;

            _context.Airports.Remove(airport);
            return await _context.SaveChangesAsync() > 0;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public async Task<List<Airport>> SearchAirports(string searchText, string filterCriteria)
    {
        Expression<Func<Airport, bool>> filterExpression = filterCriteria switch
        {
            "AirportName" => (a => a.AirportName.Contains(searchText)),
            "Country" => (a => a.Country.Contains(searchText)),
            "City" => (a => a.City.Contains(searchText)),
            "Code" => (a => a.Code.Contains(searchText)),
            "RunwaysCount" => (a => a.RunwaysCount.ToString().Contains(searchText)),
            "Founded" => (a => a.Founded.ToString().Contains(searchText)),
            _ => throw new ArgumentException("Invalid filter criteria")
        };

        return await _context.Airports.Where(filterExpression).ToListAsync();
    }

    public async Task<int> GetCountAirport()
    {
        return await _context.Airports.CountAsync();
    }
}
