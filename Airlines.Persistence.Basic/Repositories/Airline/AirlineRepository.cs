﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic;

public class AirlineRepository : IAirlineRepository
{
    private readonly AirlinesDbContext _context;

    public AirlineRepository(AirlinesDbContext context)
    {
        _context = context;
    }

    public async Task<List<Airline>> GetAirlines()
    {
        return await _context.Airlines.ToListAsync();
    }

    public async Task<Airline?> GetAirlineById(int id)
    {
        return await _context.Airlines.FirstOrDefaultAsync(a => a.Id == id);
    }

    public async Task<Airline?> GetAirlineByName(string name)
    {
        return await _context.Airlines.FirstOrDefaultAsync(a => a.Name == name);
    }

    public async Task<List<Airline>> GetAirlinesFoundedAfter(DateTime dateAfter)
    {
        return await _context.Airlines.Where(a => a.Founded > dateAfter).ToListAsync();
    }

    public async Task<List<Airline>> GetAirlinesFoundedBefore(DateTime dateBefore)
    {
        return await _context.Airlines.Where(a => a.Founded < dateBefore).ToListAsync();
    }

    public async Task<List<Airline>> GetAirlinesByFleetSize(int fleetSize)
    {
        return await _context.Airlines.Where(a => a.FleetSize == fleetSize).ToListAsync();
    }

    public async Task<bool> AddAirline(Airline airline)
    {
        try
        {
            await _context.Airlines.AddAsync(airline);
            return await _context.SaveChangesAsync() > 0;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public async Task<bool> UpdateAirline(int id, Airline airline)
    {
        var existingAirline = await _context.Airlines.FirstOrDefaultAsync(a => a.Id == id);
        if (existingAirline == null) return false;

        existingAirline.Name = airline.Name;
        existingAirline.Founded = airline.Founded;
        existingAirline.FleetSize = airline.FleetSize;
        existingAirline.Description = airline.Description;

        _context.Airlines.Update(existingAirline);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateAirlineName(int id, string name)
    {
        var airline = await _context.Airlines.FirstOrDefaultAsync(a => a.Id == id);
        if (airline == null) return false;

        airline.Name = name;
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateAirlineFounded(int id, DateTime founded)
    {
        var airline = await _context.Airlines.FirstOrDefaultAsync(a => a.Id == id);
        if (airline == null) return false;

        airline.Founded = founded;
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateAirlineFleetSize(int id, int fleetSize)
    {
        var airline = await _context.Airlines.FirstOrDefaultAsync(a => a.Id == id);
        if (airline == null) return false;

        airline.FleetSize = fleetSize;
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateAirlineDescription(int id, string description)
    {
        var airline = await _context.Airlines.FirstOrDefaultAsync(a => a.Id == id);
        if (airline == null) return false;

        airline.Description = description;
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeleteAirline(int id)
    {
        try
        {
            var airline = await _context.Airlines.FirstOrDefaultAsync(a => a.Id == id);
            if (airline == null) return false;

            _context.Airlines.Remove(airline);
            return await _context.SaveChangesAsync() > 0;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public async Task<List<Airline>> SearchAirlines(string searchText, string filterCriteria)
    {
        Expression<Func<Airline, bool>> filterExpression = filterCriteria switch
        {
            "Name" => (a => a.Name.Contains(searchText)),
            "Founded" => (a => a.Founded.ToString().Contains(searchText)),
            "FleetSize" => (a => a.FleetSize.ToString().Contains(searchText)),
            "Description" => (a => a.Description.Contains(searchText)),
            _ => throw new ArgumentException("Invalid filter criteria")
        };

        return await _context.Airlines.Where(filterExpression).ToListAsync();
    }

    public async Task<int> GetCountAirline()
    {
        return await _context.Airlines.CountAsync();
    }

}
