﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic;

public interface IAirlineRepository
{
    Task<List<Airline>> GetAirlines();
    Task<Airline?> GetAirlineById(int id);
    Task<Airline?> GetAirlineByName(string name);
    Task<List<Airline>> GetAirlinesFoundedAfter(DateTime dateAfter);
    Task<List<Airline>> GetAirlinesFoundedBefore(DateTime dateBefore);
    Task<List<Airline>> GetAirlinesByFleetSize(int fleetSize);

    Task<bool> AddAirline(Airline airline);

    Task<bool> UpdateAirline(int id, Airline airline);
    Task<bool> UpdateAirlineName(int id, string name);
    Task<bool> UpdateAirlineFounded(int id, DateTime founded);
    Task<bool> UpdateAirlineFleetSize(int id, int fleetSize);
    Task<bool> UpdateAirlineDescription(int id, string description);

    Task<bool> DeleteAirline(int id);

    Task<List<Airline>> SearchAirlines(string searchText, string filterCriteria);

    Task<int> GetCountAirline();
}
