﻿using AirlineDTO = Airlines.Business.AirlineDTO;
using AirlinesWeb.Models;

namespace AirlinesWeb.Mappers;

public static class AirlineMapper
{
    public static Airline ToModel(this AirlineDTO dto)
    {
        var model = new Airline
        {
            Id = dto.Id,
            Name = dto.Name,
            Founded = dto.Founded,
            FleetSize = dto.FleetSize,
            Description = dto.Description
        };

        return model;
    }

    public static AirlineDTO ToDTO(this Airline model)
    {
        var dto = new AirlineDTO
        {
            Id = model.Id,
            Name = model.Name,
            Founded = model.Founded,
            FleetSize = model.FleetSize,
            Description = model.Description
        };

        return dto;
    }
}
