﻿using AirportDTO = Airlines.Business.AirportDTO;
using AirlinesWeb.Models;

namespace AirlinesWeb.Mappers;

public static class AirportMapper
{
    public static Airport ToModel(this AirportDTO dto)
    {
        var model = new Airport
        {
            Id = dto.Id,
            AirportName = dto.AirportName,
            Country = dto.Country,
            City = dto.City,
            Code = dto.Code,
            RunwaysCount = dto.RunwaysCount,
            Founded = dto.Founded
        };

        return model;
    }

    public static AirportDTO ToDTO(this Airport model)
    {
        var dto = new AirportDTO
        {
            Id = model.Id,
            AirportName = model.AirportName,
            Country = model.Country,
            City = model.City,
            Code = model.Code,
            RunwaysCount = model.RunwaysCount,
            Founded = model.Founded
        };

        return dto;
    }
}
