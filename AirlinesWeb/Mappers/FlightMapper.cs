﻿using FlightDTO = Airlines.Business.FlightDTO;
using AirlinesWeb.Models;

namespace AirlinesWeb.Mappers;

public static class FlightMapper
{
    public static Flight ToModel(this FlightDTO dto)
    {
        var model = new Flight
        {
            Id = dto.Id,
            FlightNumber = dto.FlightNumber,
            FromAirportId = dto.FromAirportId,
            ToAirportId = dto.ToAirportId,
            AircraftModelId = dto.AircraftModelId,
            Price = dto.Price,
            Duration = dto.Duration,
            DepartureDatetime = dto.DepartureDatetime,
            ArrivalDatetime = dto.ArrivalDatetime,
            AirlineId = dto.AirlineId
        };

        return model;
    }

    public static FlightDTO ToDTO(this Flight model)
    {
        var dto = new FlightDTO
        {
            Id = model.Id,
            FlightNumber = model.FlightNumber,
            FromAirportId = model.FromAirportId,
            ToAirportId = model.ToAirportId,
            AircraftModelId = model.AircraftModelId,
            Price = model.Price,
            Duration = model.Duration,
            DepartureDatetime = model.DepartureDatetime,
            ArrivalDatetime = model.ArrivalDatetime,
            AirlineId = model.AirlineId
        };

        return dto;
    }
}
