using Microsoft.AspNetCore.Mvc;
using AirlinesWeb.Models;
using AirlinesWeb.Mappers;
using Airlines.Business.Service;

namespace AirlinesWeb.Controllers;

public class FlightController : Controller
{

    // GET: FlightController
    public ActionResult Index()
    {
        List<Flight> _flights = new List<Flight>();

        var flightDTOs = FlightService.GetFlightsFromDB();
        foreach (var flightDTO in flightDTOs)
        {
            _flights.Add(flightDTO.ToModel());
        }
        return View(_flights);
    }

    // POST: FlightController
    [HttpPost]
    // [ValidateAntiForgeryToken]
    public ActionResult Create(Flight flight)
    {
        try
        {
            var success = FlightService.AddFlightToDB(flight.ToDTO());
            if (!success)
            {
                System.Console.WriteLine("Error!");
                throw new Exception();
            }
            return RedirectToAction(nameof(Index));
        }
        catch
        {
            return RedirectToAction(nameof(Index));
        }
    }

    // GET: FlightSearch
    public ActionResult Search(string searchText, string filterCriteria)
    {
        List<Flight> _flights = new List<Flight>();

        if (string.IsNullOrEmpty(searchText))
            return RedirectToAction(nameof(Index));

        var flightDTOs = FlightService.SearchFlightsInDB(searchText, filterCriteria);
        foreach (var flightDTO in flightDTOs)
        {
            _flights.Add(flightDTO.ToModel());
        }
        return View("Index", _flights);
    }
}

