using Microsoft.AspNetCore.Mvc;
using AirlinesWeb.Models;
using AirlinesWeb.Mappers;
using Airlines.Business.Service;

namespace AirlinesWeb.Controllers;

public class AirportController : Controller
{

    // GET: AirportController
    public ActionResult Index()
    {
        List<Airport> _airports = new List<Airport>();
        
        var airportDTOs = AirportService.GetAirportsFromDB();
        foreach (var airportDTO in airportDTOs)
        {
            _airports.Add(airportDTO.ToModel());
        }
        return View(_airports);
    }

    // POST: AirportController
    [HttpPost]
    // [ValidateAntiForgeryToken]
    public ActionResult Create(Airport airport)
    {
        try
        {
            var success = AirportService.AddAirportToDB(airport.ToDTO());
            if (!success)
            {
                System.Console.WriteLine("Error!");
                throw new Exception();
            }
            return RedirectToAction(nameof(Index));
        }
        catch
        {
            return RedirectToAction(nameof(Index));
        }
    }

    // GET: AirportSearch
    public ActionResult Search(string searchText, string filterCriteria)
    {
        List<Airport> _airports = new List<Airport>();

        if (string.IsNullOrEmpty(searchText))
            return RedirectToAction(nameof(Index));

        var airportDTOs = AirportService.SearchAirportsInDB(searchText, filterCriteria);
        foreach (var airportDTO in airportDTOs)
        {
            _airports.Add(airportDTO.ToModel());
        }
        return View("Index", _airports);
    }
}

