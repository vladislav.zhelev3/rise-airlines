using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using AirlinesWeb.Models;
using Airlines.Business.Service;

namespace AirlinesWeb.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        var airlinesCount = AirlineService.GetAirlinesCountFromDB();
        var airportsCount = AirportService.GetAirportsCountFromDB();
        var flightsCount = FlightService.GetFlightsCountFromDB();

        ViewBag.AirlinesCount = airlinesCount!;
        ViewBag.AirportsCount = airportsCount!;
        ViewBag.FlightsCount = flightsCount!;

        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
