using Microsoft.AspNetCore.Mvc;
using AirlinesWeb.Models;
using AirlinesWeb.Mappers;
using Airlines.Business.Service;

namespace AirlinesWeb.Controllers;

public class AirlineController : Controller
{

    // GET: AirlineController
    public ActionResult Index()
    {
        List<Airline> _airlines = new List<Airline>();

        var airlineDTOs = AirlineService.GetAirlinesFromDB();
        foreach (var airlineDTO in airlineDTOs)
        {
            _airlines.Add(airlineDTO.ToModel());
        }
        return View(_airlines);
    }

    // POST: AirlineController
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create(Airline airline)
    {
        try
        {
            var success = AirlineService.AddAirlineToDB(airline.ToDTO());
            if (!success)
            {
                System.Console.WriteLine("Error!");
                throw new Exception();
            }
            return RedirectToAction(nameof(Index));
        }
        catch
        {
            return RedirectToAction(nameof(Index));
        }
    }

    // GET: AirlineSearch
    public ActionResult Search(string searchText, string filterCriteria)
    {
        List<Airline> _airlines = new List<Airline>();

        if (string.IsNullOrEmpty(searchText))
            return RedirectToAction(nameof(Index));

        var airlineDTOs = AirlineService.SearchAirlinesInDB(searchText, filterCriteria);
        foreach (var airlineDTO in airlineDTOs)
        {
            _airlines.Add(airlineDTO.ToModel());
        }
        return View("Index", _airlines);
    }

}

