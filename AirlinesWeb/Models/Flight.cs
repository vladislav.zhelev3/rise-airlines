﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace AirlinesWeb.Models;

public class Flight
{
    public int Id { get; set; }

    [Required(ErrorMessage = "FlightNumber is required")]
    public string FlightNumber { get; set; } = null!;

    [Required(ErrorMessage = "FromAirportId is required")]
    public int FromAirportId { get; set; }

    [Required(ErrorMessage = "ToAirportId is required")]
    public int ToAirportId { get; set; }

    [Required(ErrorMessage = "AircraftModelId is required")]
    public int AircraftModelId { get; set; }

    [Required(ErrorMessage = "Price is required")]
    public decimal Price { get; set; }

    [Required(ErrorMessage = "Duration is required")]
    public double Duration { get; set; }

    [Required(ErrorMessage = "DepartureDatetime is required")]
    public DateTime DepartureDatetime { get; set; }

    [Required(ErrorMessage = "ArrivalDatetime is required")]
    public DateTime ArrivalDatetime { get; set; }

    [Required(ErrorMessage = "AirlineId is required")]
    public int AirlineId { get; set; }
}
