﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace AirlinesWeb.Models;

public class Airport
{
    public int Id { get; set; }

    [Required(ErrorMessage = "Name is required")]
    public string AirportName { get; set; } = null!;

    [Required(ErrorMessage = "Country is required")]
    public string Country { get; set; } = null!;

    [Required(ErrorMessage = "City is required")]
    public string City { get; set; } = null!;

    [Required(ErrorMessage = "Code is required")]
    public string Code { get; set; } = null!;

    [Required(ErrorMessage = "RunwaysCount is required")]
    public int RunwaysCount { get; set; }

    [Required(ErrorMessage = "Founded is required")]
    public DateTime Founded { get; set; }
}
