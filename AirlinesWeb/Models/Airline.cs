﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace AirlinesWeb.Models
{
    #pragma warning disable CS8618 
    public class Airline
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Founded date is required")]
        public DateTime Founded { get; set; }

        [Required(ErrorMessage = "Fleet size is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Fleet size must be a positive number")]
        public int FleetSize { get; set; }

        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
    }
}
