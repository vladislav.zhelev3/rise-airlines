﻿using Airlines.Business;
namespace Airlines.Console;

public static class CommandLoop
{
    public static void StartCommandExperience(CommandDTO commandDto)
    {
        do
        {
            ConsoleLogger.OutputLine("( help ) Input command:");
            string command = ConsoleInput.Input();

            ICommand commandController = CommandFactory.Parse(commandDto, command)!;

            try
            {
                commandController.Execute();
            }
            catch (InputCommandException commandException)
            {
                ConsoleLogger.OutputLine(commandException.Message);
            }
        } while (true);
    }
}
