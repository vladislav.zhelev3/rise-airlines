﻿using Airlines.Business;
using Airlines.Console;

const string csvFilesPath = "../Airlines.Business/FileOperations/CSVfiles/";
const string txtFilesPath = "../Airlines.Business/FileOperations/TXTfiles/";

try
{
    List<Airport> airports = [];

    List<string> values = ReadFileReturnStringList.FileReader($"{csvFilesPath}Airport.csv");
    foreach (var item in values)
    {
        string[] value = item.Split(',');
        Airport airport = new Airport(value[0], value[1], value[2], value[3]);
        airports.Add(airport);
    }

    HashSet<string> airlineName = new HashSet<string>(ReadFileReturnStringList.FileReader($"{csvFilesPath}Airline.csv"));


    Dictionary<string, string> csvFlights = [];
    LinkedList<Flight> userFlights = [];
    values = ReadFileReturnStringList.FileReader($"{csvFilesPath}Flight.csv");

    foreach (var item in values)
    {
        string[] value = item.Split(',');
        csvFlights.Add(value[0], string.Join(",", value.Skip(1)));
    }


    List<string> csvAircraft = ReadFileReturnStringList.FileReader($"{csvFilesPath}AircraftModel.csv");


    Queue<ICommand> batchCommandQueue = new Queue<ICommand>();


    List<string> airportTreeRoot = ReadFileReturnStringList.FileReader($"{txtFilesPath}FlightTreeRoot.txt");

    Dictionary<string, string> csvFlightsForTree = [];
    values = ReadFileReturnStringList.FileReader($"{csvFilesPath}Flight_Tree.csv");

    foreach (var item in values)
    {
        string[] value = item.Split(',');
        csvFlightsForTree.Add(value[0], string.Join(",", value.Skip(1)));
    }

    var tree = new Tree<string>(airportTreeRoot[0]);
    tree.FillTreeWithAirports(csvFlightsForTree);


    DirectedGraph<Airport, Flight> airportGraph = new DirectedGraph<Airport, Flight>();

    var commandDto = new CommandDTO(airports, airlineName, csvFlights, userFlights, batchCommandQueue, csvAircraft, tree, airportGraph);

    airportGraph.FillGraphWithAirports(commandDto);


    CommandLoop.StartCommandExperience(commandDto);

}
catch (ReadingException readingException)
{
    ConsoleLogger.OutputLine(readingException.Message);
}
catch (AirlineValidationException airlineException)
{
    ConsoleLogger.OutputLine(airlineException.Message);
}
catch (AirportValidationException airportException)
{
    ConsoleLogger.OutputLine(airportException.Message);
}
catch (FlightValidationException flightException)
{
    ConsoleLogger.OutputLine(flightException.Message);
}
catch (NullOnNotNullException nullException)
{
    ConsoleLogger.OutputLine(nullException.Message);
}
