﻿namespace Airlines.Console;

public class ConsoleInput : IUserInput
{
    public static string Input() => System.Console.ReadLine()!;
}
