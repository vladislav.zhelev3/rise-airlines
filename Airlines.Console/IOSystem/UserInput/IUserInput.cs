﻿namespace Airlines.Console;

public interface IUserInput
{
    static abstract string Input();
}
