﻿namespace Airlines.Console;

public interface IUserLogger
{
    static abstract void Output(string output);
    static abstract void OutputLine(string output);
}
