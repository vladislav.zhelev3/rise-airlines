﻿namespace Airlines.Console;

public class ConsoleLogger : IUserLogger
{
    public static void Output(string output) => System.Console.Write(output);
    public static void OutputLine(string output) => System.Console.WriteLine(output);
}
